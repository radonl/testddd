<?php

namespace Tests\Feature;

use App\Domain\Interfaces\CartServiceInterface;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use League\Tactician\CommandBus;
use Symfony\Component\HttpFoundation\Cookie;
use Tests\TestCase;

/**
 * Class TestCrossSale
 * @package Tests\Feature
 */
class TestCrossSale extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var \Mockery\MockInterface
     */
    public $busMock;

    protected function setUp():void {
        parent::setUp();
        $bus = $this->mock(CommandBus::class);
        $this->app->bind('League\Tactician\CommandBus', function () use ($bus) {
            return $bus;
        });
        $this->busMock = $bus;
    }

    public function testCheckCertificate() {
        $this->busMock->shouldReceive('handle')
            ->andReturn(['test' => 'test']);
        $this->json('GET', '/api/checkout/cross-sale/')->assertJson(['test' => 'test']);

    }
}

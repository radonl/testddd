<?php

namespace Deka\CrossSale\Application\Providers;

use Deka\Common\Application\AbstractServiceProvider;
use Deka\CrossSale\Domain\Interfaces\CrossSaleServiceInterface;
use Deka\CrossSale\Domain\Services\CrossSaleService;

/**
 * Class CrossSaleProvider
 * @package Deka\CrossSale\Application\Providers
 */
class CrossSaleProvider extends AbstractServiceProvider
{
    public function register()
    {
        $this->app->singleton(CrossSaleServiceInterface::class, CrossSaleService::class);
    }

    protected $migrationsPath = __DIR__ . '/../../Infrastructure/Migrations';

    protected $routesPath = __DIR__ . '/../../UI/Routes/api.php';
}

<?php

namespace Deka\CrossSale\Application\Services\Request;

/**
 * Class GetCrossSaleProducts
 * @package Deka\CrossSale\Application\Services\Request
 */
class GetCrossSaleProducts
{
    /**
     * GetCrossSaleProducts constructor.
     */
    public function __construct(){}
}

<?php

namespace Deka\CrossSale\Application\Services;

use Deka\CrossSale\Application\Services\Request\GetCrossSaleProducts;
use Deka\CrossSale\Domain\Interfaces\CrossSaleServiceInterface;

/**
 * Class GetCrossSaleProductsHandler
 * @package Deka\CrossSale\Application\Services
 */
class GetCrossSaleProductsHandler
{
    /**
     * @var CrossSaleServiceInterface
     */
    private $crossSaleService;

    /**
     * GetCrossSaleProductsHandler constructor.
     * @param CrossSaleServiceInterface $crossSaleService
     */
    public function __construct(
        CrossSaleServiceInterface $crossSaleService
    )
    {
        $this->crossSaleService = $crossSaleService;
    }

    /**
     * @param GetCrossSaleProducts $getCrossSaleProducts
     * @return array
     */
    public function handle(GetCrossSaleProducts $getCrossSaleProducts)
    {
        return [
            'items' => $this->crossSaleService->getCrossSaleProducts()
        ];
    }
}

<?php

namespace Deka\CrossSale\Domain\Interfaces;

/**
 * Interface CrossSaleServiceInterface
 * @package Deka\CrossSale\Domain\Interfaces
 */
interface CrossSaleServiceInterface {

    /**
     * @return array
     */
    public function getCrossSaleProducts(): array;
}

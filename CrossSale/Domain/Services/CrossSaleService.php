<?php


namespace Deka\CrossSale\Domain\Services;


use Deka\CrossSale\Domain\Interfaces\CrossSaleServiceInterface;

/**
 * Class CrossSaleService
 * @package Deka\CrossSale\Domain\Services
 */
class CrossSaleService implements CrossSaleServiceInterface {

    /**
     * @return array
     */
    public function getCrossSaleProducts(): array
    {
        // TODO: Mock
        return [[
            "id" => 305592,
            "name" => "Футляр Beco 324184 для транспортировки часов",
            "old_price" => 10000,
            "price" => 8000,
            "discount_percent" => 20,
            "image" => "/img.png"
        ], [
            "id" => 305594,
            "name" => "Футляр Beco 3241324 для транспортировки часов",
            "old_price" => 10000,
            "price" => 7000,
            "discount_percent" => 30,
            "image" => "/img.png"
        ], [
            "id" => 305567,
            "name" => "Футляр Beco 324134 для транспортировки часов",
            "old_price" => 8000,
            "price" => 8000,
            "discount_percent" => 0,
            "image" => "/img.png"
        ]];
    }
}

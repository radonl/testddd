<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->middleware('api')
    ->namespace("Deka\CrossSale\UI\Controllers")
    ->group(function () {
        Route::post('/checkout/cross-sale/', "CrossSaleController@getProducts");
    });

<?php

namespace Deka\CrossSale\UI\Controllers;


use Deka\CrossSale\Application\Services\Request\GetCrossSaleProducts;
use Illuminate\Routing\Controller;
use League\Tactician\CommandBus;

/**
 * Class CrossSaleController
 * @package Deka\CrossSale\UI\Controllers
 */
class CrossSaleController extends Controller
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CommentsController constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus) {
        $this->bus = $bus;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts() {
        return response()->json($this->bus->handle(new GetCrossSaleProducts()));
    }
}

<?php

namespace Deka\User\Application\Providers;

use Deka\Common\Application\AbstractServiceProvider;
use Deka\User\Domain\Interfaces\UserServiceInterface;
use Deka\User\Domain\Services\UserService;

/**
 * Class UserProvider
 * @package Deka\User\Application\Providers
 */
class UserProvider extends AbstractServiceProvider
{
    public function register()
    {
        $this->app->singleton(UserServiceInterface::class, UserService::class);
    }

    protected $migrationsPath = __DIR__ . '/../../Infrastructure/Migrations';

    protected $routesPath = __DIR__ . '/../../UI/Routes/api.php';
}

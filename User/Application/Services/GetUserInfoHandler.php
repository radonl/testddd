<?php

namespace Deka\User\Application\Services;

use Deka\User\Application\Services\Request\GetUserInfo;
use Deka\User\Domain\Interfaces\UserServiceInterface;

/**
 * Class GetUserInfoHandler
 * @package Deka\User\Application\Services
 */
class GetUserInfoHandler
{
    /**
     * @var UserServiceInterface
     */
    private $userService;

    /**
     * GetUserInfoHandler constructor.
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param GetUserInfo $request
     * @return array
     */
    public function handle(GetUserInfo $request) : array
    {
        return $this->userService->parse($request->getJwt());
    }
}

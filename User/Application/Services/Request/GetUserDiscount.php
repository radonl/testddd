<?php

namespace Deka\User\Services\Request;

use Deka\User\Application\Validation\GetUserDiscountRequestValidator;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class GetUserDiscount
 * @package Deka\User\Services\Request
 */
class GetUserDiscount {

    /**
     * @var string
     */
    private $phone;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * GetUserDiscount constructor.
     * @param array $request
     */
    public function __construct(array $request)
    {
        $this->phone = $request['phone'] ?? null;
        $this->validator = GetUserDiscountRequestValidator::make($this->toArray());
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            'phone' => $this->phone
        ];
    }

    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }
}

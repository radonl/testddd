<?php

namespace Deka\User\Application\Services\Request;

/**
 * Class GetUserInfo
 * @package Deka\User\Application\Services\Request
 */
class GetUserInfo {

    /**
     * @var string
     */
    private $jwt;

    /**
     * GetUserInfo constructor.
     * @param string $jwt
     */
    public function __construct(string $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            'jwt' => $this->jwt
        ];
    }

    /**
     * @return string
     */
    public function getJwt(): string
    {
        return $this->jwt;
    }
}

<?php

namespace Deka\User\Services;

use Deka\User\Domain\Interfaces\UserServiceInterface;
use Deka\User\Services\Request\GetUserDiscount;

/**
 * Class GetUserDiscountHandler
 * @package Deka\User\Services
 */
class GetUserDiscountHandler
{
    /**
     * @var UserServiceInterface
     */
    private $userService;

    /**
     * GetUserDiscountHandler constructor.
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param GetUserDiscount $request
     * @return array
     */
    public function handle(GetUserDiscount $request) : array
    {
        return [
            'status' => true,
            'json' =>  $this->userService->getDiscount($request->getPhone())
        ];
    }
}

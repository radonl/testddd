<?php

namespace Deka\User\Application\Validation;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

/**
 * Class GetUserDiscountRequestValidator
 * @package Deka\User\Application\Validation
 */
class GetUserDiscountRequestValidator extends FormRequest
{
    const RULES = [
        'phone' => 'required|string|regex:/^\+380\(\d{2}\)\d{3}\-\d{2}\-\d{2}$/'
    ];

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($request){
        return Validator::make($request, GetUserDiscountRequestValidator::RULES);
    }
}

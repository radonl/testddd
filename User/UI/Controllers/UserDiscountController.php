<?php

namespace Deka\User\UI\Controllers;

use Deka\User\Services\Request\GetUserDiscount;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use League\Tactician\CommandBus;

/**
 * Class UserDiscountController
 * @package Deka\User\UI\Controllers
 */
class UserDiscountController extends Controller
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CommentsController constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus) {
        $this->bus = $bus;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserDiscount(Request $request)
    {
        $request = new GetUserDiscount($request->toArray());
        if($request->getValidator()->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $request->getValidator()->errors()
            ], 422);
        } else {
            return response()->json($this->bus->handle($request)['json'], 200);
        }
    }
}

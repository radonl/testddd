<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->middleware('api')
    ->namespace("Deka\User\UI\Controllers")
    ->group(function () {
        Route::post('/checkout/order/user-discount/', "UserDiscountController@getUserDiscount");
    });

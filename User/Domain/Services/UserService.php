<?php


namespace Deka\User\Domain\Services;

use Deka\User\Domain\Interfaces\UserServiceInterface;

/**
 * Class UserService
 * @package Deka\User\Domain\Services
 */
class UserService implements UserServiceInterface {

    /**
     * @param string $jwt
     * @return array
     */
    public function parse(string $jwt): array
    {
        //ToDo should return user from db by id (received from jwt)
        return [
            'user' => [
                'id' => '8978',
                'name' => 'Джон Энтони Бёрджесс Уилсонс',
                'phone' => '+380(99)123-45-67',
                'email' => 'ClockworkOrange@gmail.com',
                'city' => [
                    'id' => 1,
                    'name' => 'Днепр',
                    'code' => 'Dnepr'
                ],
            ],
            'discount-card' => [
                "accumulated-amount" => 99999,
                "discount-percent" => 10,
                "number-comment" => "Может не быть, тогда выводить id клиента.",
                "number" => 454545454545,
                "next-discount" => [
                    "percent" => 15,
                    "left" => 13043
                ]
            ]
        ];
    }

    /**
     * @param string $phone
     * @return array
     */
    public function getDiscount(string $phone) : array
    {
        //ToDo should return next data from db
        return [
            "user" => [
                  "discount" => 10
            ],
            "info" => [
                "discount_percent" => 87,
                "count" => 3,
                "total" => 16900
            ]
        ];
    }
}

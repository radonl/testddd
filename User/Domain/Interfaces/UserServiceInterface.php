<?php

namespace Deka\User\Domain\Interfaces;

/**
 * Interface UserServiceInterface
 * @package Deka\User\Domain\Interfaces
 */
interface UserServiceInterface {
    /**
     * @param string $jwt
     * @return array
     */
    public function parse(string $jwt): array;

    /**
     * @param string $phone
     * @return array
     */
    public function getDiscount(string $phone): array;
}

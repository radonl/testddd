<?php

namespace Tests\Unit;

use App\UI\Requests\GetUserDiscountValidator;
use Faker\Factory;
use Tests\TestCase;

/**
 * Class TestsUnitAddProductValidation
 * @package Tests\Unit
 */
class TestGetUserDiscountRequestValidation extends TestCase
{
    /**
     * @var array $rules
     */
    private $rules;

    /**
     * @var \Illuminate\Validation\Validator $validator
     */
    private $validator;

    private $data = [
        'phone' => '+380(11)111-11-11',
    ];

    protected function setUp():void {
        parent::setUp();
        $this->validator = app()->get('validator');
        $this->rules = GetUserDiscountValidator::RULES;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function validationProvider() {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);
        return [
            'request.success' => [
                'passed' => true,
                'request' => $this->data
            ],
            'phone.validation.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['phone' => 12345])
            ],
            'phone.validation.regex' => [
                'passed' => false,
                'request' => array_merge($this->data, ['phone' => '+380'])
            ],
            'phone.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data,['phone' =>  '+380(11)111-11-11'])
            ]
        ];
    }

    /**
     * @test
     * @dataProvider validationProvider
     * @param bool $shouldPass
     * @param array $mockedRequestData
     */
    public function validation_results_as_expected($shouldPass, $mockedRequestData)
    {
        $this->assertEquals(
            $shouldPass,
            $this->validate($mockedRequestData)
        );
    }

    /**
     * @param $mockedRequestData
     * @return mixed
     */
    protected function validate($mockedRequestData)
    {
        return $this->validator
            ->make($mockedRequestData, $this->rules)
            ->passes();
    }
}

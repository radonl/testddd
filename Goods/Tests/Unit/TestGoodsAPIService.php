<?php

namespace Tests\Unit;

use Deka\Goods\Domain\Interfaces\GoodsAPIServiceInterface;
use Tests\TestCase;

/**
 * Class TestCartService
 * @package Tests\Unit
 */
class TestGoodsAPIService extends TestCase
{
    /**
     * @var \Deka\Goods\Domain\Services\GoodsAPIService
     */
    private $service;

    /**
     * @var \Mockery\MockInterface
     */
    private $guzzleMock;

    /**
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp():void {
        parent::setUp();
        $guzzleMock = $this->mock( \GuzzleHttp\Client::class);
        $this->app->bind('\GuzzleHttp\Client', function () use ($guzzleMock) {
            return $guzzleMock;
        });
        $this->guzzleMock = $guzzleMock;
        $this->service = $this->app->make('Deka\Goods\Domain\Interfaces\GoodsAPIServiceInterface');
    }

    public function testGetProduct() {
        $this->guzzleMock
            ->shouldReceive('getBody')
            ->andReturn('[{"test":1}]');
        $this->guzzleMock
            ->shouldReceive('request')
            ->with('get', 'http://dev.deka.ua/api/goods/goods-ids/1')
            ->andReturn($this->guzzleMock);

        $this->assertTrue($this->service->getProduct(1)['test'] == 1);
    }
}

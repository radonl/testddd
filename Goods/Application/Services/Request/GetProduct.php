<?php

namespace Deka\Goods\Application\Services\Request;

/**
 * Class GetProduct
 * @package Deka\Goods\Application\Services\Request
 */
class GetProduct
{
    /**
     * @var int
     */
    private $g_id;

    /**
     * GetProduct constructor.
     * @param int $g_id
     */
    public function __construct(int $g_id) {
        $this->g_id = $g_id;
    }

    /**
     * @return int
     */
    public function getGId()
    {
        return $this->g_id;
    }
}

<?php

namespace Deka\Goods\Application\Services;

use Deka\Goods\Application\Services\Request\GetProduct;
use Deka\Goods\Domain\Interfaces\GoodsAPIServiceInterface;

/**
 * Class GetProductHandler
 * @package Deka\Goods\Application\Services
 */
class GetProductHandler
{

    /**
     * @var GoodsAPIServiceInterface
     */
    private $goodsAPIService;

    /**
     * GetProductHandler constructor.
     * @param GoodsAPIServiceInterface $goodsAPIService
     */
    public function __construct(GoodsAPIServiceInterface $goodsAPIService)
    {
        $this->goodsAPIService = $goodsAPIService;
    }

    /**
     * @param GetProduct $request
     * @return array
     * @throws \Exception
     */
    public function handle(GetProduct $request)
    {
        return $this->goodsAPIService->getProduct($request->getGId());
    }
}

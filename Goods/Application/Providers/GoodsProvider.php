<?php

namespace Deka\Goods\Application\Providers;

use Deka\Common\Application\AbstractServiceProvider;
use Deka\Goods\Domain\Interfaces\GoodsAPIServiceInterface;
use Deka\Goods\Domain\Services\GoodsAPIService;

/**
 * Class GoodsProvider
 * @package Deka\Goods\Application\Providers
 */
class GoodsProvider extends AbstractServiceProvider
{
    public function register()
    {
        $this->app->singleton(GoodsAPIServiceInterface::class, GoodsAPIService::class);
    }

    protected $migrationsPath = __DIR__ . '/../../Infrastructure/Migrations';

    protected $routesPath = __DIR__ . '/../../UI/Routes/api.php';
}

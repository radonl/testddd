<?php

namespace Deka\Goods\Domain\Interfaces;

/**
 * Interface GoodsAPIServiceInterface
 * @package Deka\Goods\Domain\Interfaces
 */
interface GoodsAPIServiceInterface {

    /**
     * @param int $g_id
     * @return array
     */
    public function getProduct(int $g_id): array;
}

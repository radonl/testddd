<?php


namespace Deka\Goods\Domain\Services;


use Deka\Goods\Domain\Interfaces\GoodsAPIServiceInterface;
use Exception;
use GuzzleHttp\Client;

/**
 * Class GoodsAPIService
 * @package Deka\Goods\Domain\Services
 */
class GoodsAPIService implements GoodsAPIServiceInterface {

    /**
     * @var Client
     */
    private $guzzle;

    /**
     * GoodsAPIService constructor.
     * @param Client $guzzle
     */
    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    /**
     * @param int $g_id
     * @return array
     * @throws Exception
     */
    public function getProduct(int $g_id): array
    {
        $response = json_decode(
            $this->guzzle->request('get', "http://dev.deka.ua/api/goods/goods-ids/{$g_id}")->getBody(),
            true
        );
        if(empty($response)) {
            throw new Exception('Product does not exist');
        } else {
            return $response[0];
        }
    }
}

<?php

namespace Deka\Common\Traits;


/**
 * Trait PropertiesSetter
 * @package App\Common\Traits
 */
trait PropertiesSetter {

    /**
     * PropertiesSetter constructor.
     * @param array $state
     */
    public function __construct($state = [])
    {
        foreach ($state as $key => $value) {
            if (method_exists($this, ($method = 'set'.strtr($key, ['_' => ''])))) {
                call_user_func([$this, $method], $value);
            } else if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

}

<?php

namespace Deka\Common\Traits;


/**
 * Trait UrlSanitiser
 * @package Deka\Common\Traits
 */
trait UrlSanitiser {

    private $special_symbols = [' ' => '-', '_' => '-', '&' => '-', '/' => '-', '\\' => '-', '|' => '-'];

    /**
     * @param string $url
     * @return null|string|string[]
     */
    private function sanitizeUrl(string $url) {
        return preg_replace(['/[^a-z0-9-]/', '/-+/'], ['', '-'], strtr(strtolower($url), $this->special_symbols));
    }

}

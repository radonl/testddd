<?php


namespace Deka\Common\Helpers;


use Closure;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Facades\Cache as Origin;

/**
 * Class Cache
 * @package Deka\Common\Helpers
 */
class Cache
{
    /**
     * @param string $key
     * @param Closure $closure
     * @param int|null $ttl
     * @return mixed
     */
    public static function remember(string $key, Closure $closure, int $ttl = null) {
        return Origin::remember($key, $ttl ?? config('deka.cache.hour'), $closure);
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public static function get(string $key, $default = null) {
        return Origin::get($key, $default);
    }

    /**
     * @param string $key
     * @param $value
     * @param $ttl
     * @return void
     */
    public static function put(string $key, $value, $ttl): void {
        Origin::put($key, $value, $ttl);
    }

    /**
     * @param string $key
     * @return bool
     */
    public static function has(string $key): bool {
        return Origin::has($key);
    }

    /**
     * @return Repository
     */
    public static function store(): Repository {
        return Origin::store();
    }

    /**
     * @param string $key
     * @return bool
     */
    public static function forget(string $key): bool {
        return Origin::forget($key);
    }

    /**
     * @param string $key
     * @param Closure $closure
     * @return mixed
     */
    public static function day(string $key, Closure $closure) {
        return self::remember($key, $closure, config('deka.cache.day'));
    }

    /**
     * @param string $key
     * @param Closure $closure
     * @return mixed
     */
    public static function hour(string $key, Closure $closure) {
        return self::remember($key, $closure, config('deka.cache.hour'));
    }

    /**
     * @param string $key
     * @param Closure $closure
     * @return mixed
     */
    public static function minute(string $key, Closure $closure) {
        return self::remember($key, $closure, config('deka.cache.minute'));
    }
}

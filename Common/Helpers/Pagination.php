<?php

namespace Deka\Common\Helpers;

/**
 * Class Pagination
 *
 * @package App\Common\Helpers
 */
class Pagination {

    const PER_PAGE = 21;

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $perPage = self::PER_PAGE;

    /**
     * @var int
     */
    private $totalItems;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @param int $page
     * @return Pagination
     */
    public function setPage(int $page) {
        $this->page = $page;
        return $this;
    }

    /**
     * @param int $perPage
     * @return Pagination
     */
    public function setPerPage(int $perPage) {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @param int $totalItems
     * @return Pagination
     */
    public function setTotalItems(int $totalItems) {
        $this->totalItems = $totalItems;
        return $this;
    }

    /**
     * @param string $baseUrl
     * @return Pagination
     */
    public function setBaseUrl(string $baseUrl) {
        $this->baseUrl = $baseUrl;
        return $this;
    }

    /**
     * @return int
     */
    private function page() {
        if ($this->page > $this->totalPages() || $this->page <= 0) {
            return 1;
        } else {
            return $this->page;
        }
    }

    /**
     * @return float|int
     */
    public function offset() {
        return (($this->currentPage() - 1) * $this->perPage());
    }

    /**
     * @param $i
     * @param $j
     *
     * @return array
     */
    private function pageUrls($i, $j) {
        $urls = [];
        for ($i; $i <= $j; $i++) {
            if ($i <= 0) {
                continue;
            }
            if ($i > $this->totalPages()) {
                continue;
            }
            $urls[$i] = $this->pageUrl($i);
        }
        return $urls;
    }

    /**
     * @return bool
     */
    public function inRange() : bool {
        return $this->totalPages() >= $this->page;
    }

    /**
     * @return int
     */
    public function totalPages() {
        return (int) ceil($this->totalItems() / $this->perPage());
    }

    /**
     * @return null|string|string[]
     */
    private function baseUrl() {
        if(!empty($this->baseUrl)){
            return $this->baseUrl;
        }

        $path = '/'.request()->path().'/';
        preg_match('/\/page_(\d+)/', $path, $matches);

        return $this->baseUrl = empty($matches) ? $path : preg_replace('/\/page_\d+\/?/', '/', $path);
    }

    /**
     * @return int
     */
    public function currentPage() {
        return $this->page();
    }

    /**
     * @return int
     */
    public function perPage() {
        return $this->perPage;
    }

    /**
     * @return int
     */
    public function totalItems() {
        return $this->totalItems;
    }

    /**
     * @return bool
     */
    public function hasMorePages(): bool {
        return $this->currentPage() < $this->totalPages();
    }

    /**
     * @return null|string|string[]
     */
    public function currentUrl() {
        return $this->pageUrl($this->currentPage());
    }

    /**
     * @param int $page
     *
     * @return null|string|string[]
     */
    public function pageUrl(int $page) {
        if ($page <= 1) {
            return $this->baseUrl();
        }
        if ($page > $this->totalPages()) {
            return "{$this->baseUrl()}page_{$this->totalPages()}/";
        }
        return "{$this->baseUrl()}page_$page/";
    }

    /**
     * @return null|string|string[]
     */
    public function firstUrl() {
        return $this->pageUrl(1);
    }

    /**
     * @return null|string|string[]
     */
    public function lastUrl() {
        return $this->pageUrl($this->totalPages());
    }

    /**
     * @return null|string|string[]
     */
    public function nextUrl() {
        if ($this->currentPage() + 1 > $this->totalPages()) {
            return null;
        }
        return $this->pageUrl($this->currentPage() + 1);
    }

    /**
     * @return null|string|string[]
     */
    public function prevUrl() {
        if ($this->currentPage() - 1 < 1) {
            return null;
        }
        return $this->pageUrl($this->currentPage() - 1);
    }

    /**
     * @return array
     */
    public function listUrls() {
        $leftOffset = $this->param['leftOffset'] ?? 2;
        $rightOffset = $this->param['rightOffset'] ?? 2;

        if ($this->currentPage() <= $leftOffset) {
            $urls = $this->pageUrls(1, $rightOffset+$rightOffset+1);
        } else if ($this->currentPage() >= ($this->totalPages()-$rightOffset)) {
            $urls = $this->pageUrls((($this->totalPages()-$rightOffset)-$rightOffset),$this->totalPages());
        } else {
            $urls = $this->pageUrls($this->currentPage() - $leftOffset, $this->currentPage() + $rightOffset);
        }

        return $urls;
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            'perPage'      => $this->perPage(),
            'totalItems'   => $this->totalItems(),
            'currentPage'  => $this->currentPage(),
            'totalPages'   => $this->totalPages(),
            'hasMorePages' => $this->hasMorePages(),
            'indexing'     => $this->currentPage() === 1,
            'redirectToFirst' => $this->totalPages() > 1 && $this->page > $this->totalPages(),
            'links'        => $this->links()
        ];
    }

    /**
     * @return array
     */
    public function links() {
        return [
            'current' => $this->currentUrl(),
            'first'   => $this->firstUrl(),
            'last'    => $this->lastUrl(),
            'next'    => $this->nextUrl(),
            'prev'    => $this->prevUrl(),
            'list'    => $this->listUrls()
        ];
    }
}

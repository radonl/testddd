<?php
/**
 * Created by PhpStorm.
 * User: laa
 * Date: 8/6/19
 * Time: 11:52 AM
 */

namespace Deka\Common\Helpers;


/**
 * Class PropertySetter
 * @package App\Common\Helpers
 * @deprecated
 */
abstract class PropertySetter {

    /**
     * PropertySetter constructor.
     * @param array $attributes
     */
    public function __construct($attributes = []) {
        if (!empty($attributes)) {
            foreach ($attributes as $name => $value) {
                if (property_exists($this, $name)) {
                    $this->$name = $value;
                }
            }
        }
    }

}

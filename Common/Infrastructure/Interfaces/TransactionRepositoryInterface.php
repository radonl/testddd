<?php

namespace Deka\Common\Infrastructure\Interfaces;

/**
 * Interface TransactionRepositoryInterface
 * @package Deka\Common\Infrastructure\Interfaces
 */
interface TransactionRepositoryInterface {

    public function startTransaction() : void;

    public function commit() : void;

    public function rollBack() : void;
}

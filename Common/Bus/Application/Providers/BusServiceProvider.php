<?php
/**
 * Created by PhpStorm.
 * User: laa
 * Date: 8/5/19
 * Time: 9:53 AM
 */

namespace Deka\Common\Bus\Application\Providers;


use Illuminate\Support\ServiceProvider;
use League\Tactician\CommandBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\CommandNameExtractor\CommandNameExtractor;
use League\Tactician\Handler\Locator\CallableLocator;
use League\Tactician\Handler\Locator\HandlerLocator;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use League\Tactician\Handler\MethodNameInflector\MethodNameInflector;

/**
 * Class BusServiceProvider
 * @package Deka\Common\Bus\Application\Providers
 */

/**
 * Class BusServiceProvider
 * @package Deka\Common\Bus\Application\Providers
 */
class BusServiceProvider extends ServiceProvider {

    public $bindings = [
        CommandNameExtractor::class => ClassNameExtractor::class,
        HandlerLocator::class => CallableLocator::class,
        MethodNameInflector::class => HandleInflector::class
    ];

    public function register() {
        $this->app->when(CommandBus::class)->needs('$middleware')->give(function () {
            return [$this->app->make(CommandHandlerMiddleware::class)];
        });

        $this->app->when(CallableLocator::class)->needs('$callable')->give(function () {
            return function($class_name){
                return $this->app->make(strtr($class_name, [
                        'Request\\' => ''
                    ]) . 'Handler');
            };
        });
    }

}

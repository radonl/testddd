<?php

namespace Deka\Common\Application;

use Illuminate\Support\ServiceProvider;

/**
 * Class AbstractServiceProvider
 * @package App\Common\Application
 */
abstract class AbstractServiceProvider extends ServiceProvider{
    /**
     * @var string
     */
    protected $migrationsPath;
    /**
     * @var string
     */
    protected $routesPath;
    /**
     * Console commands list
     * @var array
     */
    protected $commands = [];
    /**
     * @var array
     */
    protected $dependencies = [];
    /**
     * @var array
     */
    protected $lang_repositories = [];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        if(!empty($this->migrationsPath)){
            $this->loadMigrationsFrom($this->migrationsPath);
        }

        if(!empty($this->routesPath)){
            $this->loadRoutesFrom($this->routesPath);
        }

        if ($this->app->runningInConsole() && !empty($this->commands)) {
            $this->commands($this->commands);
        }

        foreach ($this->dependencies as $dependency){
            $this->app->when($dependency['when'])
                ->needs($dependency['need'])
                ->give($dependency['give']);
        }

        foreach ($this->lang_repositories as $need => $langRepositories){
            $this->app->singleton($need, $langRepositories[$this->app->getLocale()]);
        }
    }

}

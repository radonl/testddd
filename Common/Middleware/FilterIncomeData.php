<?php

namespace Deka\Common\Middleware;

use \Illuminate\Http\Request;
use Closure;

/**
 * Class FilterIncomeData
 * @package App\Common\Middleware
 */
class FilterIncomeData {

    private $functions = ['trim', 'strip_tags', 'htmlspecialchars'];

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        $route = $request->route();
        foreach ($route->parameters() as $key => $value){
            $route->setParameter($key, $this->clear($value));
        }

        return $next($request);
    }

    /**
     * @param $value
     * @return string
     */
    private function clear($value): string {
        foreach ($this->functions as $function) {
            $value = $function($value);
        }

        return $value;
    }

}

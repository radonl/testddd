<?php
/**
 * Created by PhpStorm.
 * User: laa
 * Date: 8/2/19
 * Time: 11:40 AM
 */

namespace Deka\Common\Exceptions;


use Exception;

/**
 * Class NotFoundException
 * @package Deka\Common\Exceptions
 */
class NotFoundException extends Exception {

}

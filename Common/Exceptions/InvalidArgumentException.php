<?php

namespace Deka\Common\Exceptions;


use Exception;

/**
 * Class InvalidArgumentException
 * @package Deka\Common\Exceptions
 */
class InvalidArgumentException extends Exception {

}

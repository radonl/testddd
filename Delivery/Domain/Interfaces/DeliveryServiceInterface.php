<?php

namespace Deka\Delivery\Domain\Interfaces;

use Illuminate\Support\Collection;

/**
 * Interface DeliveryServiceInterface
 * @package Deka\Delivery\Domain\Interfaces
 */
interface DeliveryServiceInterface {

    /**
     * @param Collection $products
     * @param object $delivery
     * @return bool
     */
    public function isApplicableToCart(Collection $products, object $delivery): bool;

    /**
     * @param Collection $products
     * @param object $delivery
     * @return string
     */
    public function getPriceOfDelivery(Collection $products, object $delivery): string;

    /**
     * @param int $deliveryServiceId
     * @return array
     */
    public function getCities(int $deliveryServiceId): array;

    /**
     * @param int $deliveryServiceId
     * @param int $cityId
     * @return array
     */
    public function getBranches(int $deliveryServiceId, int $cityId): array;

    /**
     * @param Collection $products
     * @return Collection
     */
    public function getDeliveryServices(Collection $products): Collection;
}

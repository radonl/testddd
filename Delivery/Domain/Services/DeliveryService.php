<?php

namespace Deka\Delivery\Domain\Services;

use Deka\Delivery\Domain\Interfaces\DeliveryServiceInterface;
use Deka\Delivery\Infrastructure\Interfaces\DeliveryReadRepositoryInterface;
use Illuminate\Support\Collection;

/**
 * Class DeliveryService
 * @package Deka\Delivery\Domain\Services
 */
class DeliveryService implements DeliveryServiceInterface {

    private $deliveryReadRepository;

    /**
     * DeliveryService constructor.
     * @param DeliveryReadRepositoryInterface $deliveryReadRepository
     */
    public function __construct(
        DeliveryReadRepositoryInterface $deliveryReadRepository
    )
    {
        $this->deliveryReadRepository = $deliveryReadRepository;
    }

    /**
     * @param Collection $products
     * @param $delivery
     * @return bool
     */
    public function isApplicableToCart(Collection $products, object $delivery) :bool
    {
        return !$this->deliveryReadRepository->brandsIsBlocked(
            collect($products->toArray())->pluck('brandId')->unique()->toArray(), $delivery->id
        );
    }

    /**
     * @param Collection $products
     * @param object $delivery
     * @return string
     */
    public function getPriceOfDelivery(Collection $products, object $delivery): string
    {
        return $this->deliveryReadRepository->getPriceOfDelivery(
            collect($products->toArray())->pluck('categoryId')->unique()->toArray(),
            $delivery->id
        );
    }

    /**
     * @param int $deliveryServiceId
     * @return array
     */
    public function getCities(int $deliveryServiceId): array
    {
        return $this->deliveryReadRepository->getCities($deliveryServiceId);
    }

    /**
     * @param int $deliveryServiceId
     * @param int $cityId
     * @return array
     */
    public function getBranches(int $deliveryServiceId, int $cityId): array
    {
        return $this->deliveryReadRepository->getBranches($deliveryServiceId, $cityId);
    }

    /**
     * @param Collection $products
     * @return Collection
     */
    public function getDeliveryServices(Collection $products): Collection
    {
        return $this->deliveryReadRepository->getAvailableDeliveryServices(
            collect($products->toArray())->pluck('brandId')->unique()->toArray()
        );
    }
}

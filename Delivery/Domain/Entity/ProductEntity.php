<?php

namespace Deka\Delivery\Domain\Entity;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Class ProductEntity
 * @package Deka\Delivery\Domain\Entity
 */
class ProductEntity implements Arrayable{

    /**
     * @var int
     */
    private $price;

    /**
     * @var int
     */
    private $brandId;

    /**
     * @var int|null
     */
    private $categoryId;

    /**
     * @var int|null
     */
    private $count;

    /**
     * ProductEntity constructor.
     * @param $item
     */
    public function __construct($item)
    {
        $item = $item->toArray();
        $this->price = $item['price'];
        $this->brandId = $item['brandId'];
        $this->categoryId = $item['categoryId'];
        $this->count = $item['count'];
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
          'price' => $this->price,
          'count' => $this->count,
          'categoryId' => $this->categoryId,
          'brandId' => $this->brandId
        ];
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    /**
     * @return int
     */
    public function getBrandId(): int
    {
        return $this->brandId;
    }

    /**
     * @return int
     */
    public function getTotal(): int {
        return $this->count * $this->price;
    }
}

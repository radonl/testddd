<?php

namespace Deka\Delivery\Application\Services\Request;

use Deka\Delivery\Application\Validation\GetCitiesValidator;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class GetCities
 * @package Deka\Delivery\Application\Services\Request
 */
class GetCities {

    /**
     * @var int
     */
    private $deliveryServiceId;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * GetCities constructor.
     * @param int $deliveryServiceId
     */
    public function __construct(int $deliveryServiceId)
    {
        $this->deliveryServiceId = $deliveryServiceId;
        $this->validator = GetCitiesValidator::make($this->toArray());
    }

    /**
     * @return int
     */
    public function getDeliveryServiceId(): int
    {
        return $this->deliveryServiceId;
    }

    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }

    /**
     * @return array
     */
    private function toArray(): array
    {
        return [
          'deliveryServiceId' => $this->deliveryServiceId
        ];
    }
}

<?php

namespace Deka\Delivery\Application\Services\Request;

use Deka\Delivery\Application\Validation\SelectDeliveryValidator;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class SelectDelivery
 * @package Deka\Delivery\Application\Services\Request
 */
class SelectDelivery {

    /**
     * @var int
     */
    private $id;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * SelectDelivery constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
        $this->validator = SelectDeliveryValidator::make($this->toArray());
    }

    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    private function toArray()
    {
        return [
            'id' => $this->id
        ];
    }
}

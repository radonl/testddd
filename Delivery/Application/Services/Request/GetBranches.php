<?php

namespace Deka\Delivery\Application\Services\Request;

use Deka\Delivery\Application\Validation\GetBranchesValidator;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class GetBranches
 * @package Deka\Delivery\Application\Services\Request
 */
class GetBranches {

    /**
     * @var int
     */
    private $deliveryServiceId;

    /**
     * @var int
     */
    private $cityId;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * GetCities constructor.
     * @param int $deliveryServiceId
     * @param int $cityId
     */
    public function __construct(int $deliveryServiceId, int $cityId)
    {
        $this->deliveryServiceId = $deliveryServiceId;
        $this->cityId = $cityId;
        $this->validator = GetBranchesValidator::make($this->toArray());
    }

    /**
     * @return int
     */
    public function getDeliveryServiceId(): int
    {
        return $this->deliveryServiceId;
    }

    /**
     * @return int
     */
    public function getCityId(): int
    {
        return $this->cityId;
    }

    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }

    /**
     * @return array
     */
    private function toArray(): array
    {
        return [
            'deliveryServiceId' => $this->deliveryServiceId,
            'cityId' => $this->cityId
        ];
    }
}

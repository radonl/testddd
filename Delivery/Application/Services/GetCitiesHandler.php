<?php

namespace Deka\Delivery\Application\Services;

use Deka\Delivery\Application\Services\Request\GetCities;
use Deka\Delivery\Domain\Interfaces\DeliveryServiceInterface;

/**
 * Class GetCitiesHandler
 * @package Deka\Delivery\Application\Services
 */
class GetCitiesHandler
{
    /**
     * @var DeliveryServiceInterface
     */
    private $deliveryService;

    /**
     * GetCitiesHandler constructor.
     * @param DeliveryServiceInterface $deliveryService
     */
    public function __construct(DeliveryServiceInterface $deliveryService)
    {
        $this->deliveryService = $deliveryService;
    }

    /**
     * @param GetCities $request
     * @return array
     */
    public function handle(GetCities $request): array
    {
        return [
            'cities' => $this->deliveryService->getCities($request->getDeliveryServiceId())
        ];
    }
}

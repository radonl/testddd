<?php

namespace Deka\Delivery\Application\Services;

use Deka\Delivery\Application\Services\Request\GetBranches;
use Deka\Delivery\Domain\Interfaces\DeliveryServiceInterface;

/**
 * Class GetBranchesHandler
 * @package Deka\Delivery\Application\Services
 */
class GetBranchesHandler
{
    /**
     * @var DeliveryServiceInterface
     */
    private $deliveryService;

    /**
     * GetCitiesHandler constructor.
     * @param DeliveryServiceInterface $deliveryService
     */
    public function __construct(DeliveryServiceInterface $deliveryService)
    {
        $this->deliveryService = $deliveryService;
    }


    /**
     * @param GetBranches $request
     * @return array
     */
    public function handle(GetBranches $request)
    {
        return [
            'branches' => $this->deliveryService->getBranches($request->getDeliveryServiceId(), $request->getCityId())
        ];
    }
}

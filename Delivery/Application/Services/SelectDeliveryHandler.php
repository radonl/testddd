<?php

namespace Deka\Delivery\Application\Services;

use Deka\Cart\Services\Request\GetAllProducts;
use Deka\Delivery\Application\Services\Request\SelectDelivery;
use Deka\Delivery\Domain\Entity\ProductEntity;
use Deka\Delivery\Domain\Interfaces\DeliveryServiceInterface;
use Deka\Delivery\Infrastructure\Interfaces\DeliveryReadRepositoryInterface;
use Exception;
use League\Tactician\CommandBus;

/**
 * Class SelectDeliveryHandler
 * @package Deka\Delivery\Application\Services
 */
class SelectDeliveryHandler
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * @var DeliveryReadRepositoryInterface
     */
    private $deliveryReadRepository;

    /**
     * @var DeliveryServiceInterface
     */
    private $deliveryService;

    /**
     * SelectDeliveryHandler constructor.
     * @param DeliveryReadRepositoryInterface $deliveryReadRepository
     * @param DeliveryServiceInterface $deliveryService
     * @param CommandBus $bus
     */
    public function __construct(
        DeliveryReadRepositoryInterface $deliveryReadRepository,
        DeliveryServiceInterface $deliveryService,
        CommandBus $bus
    )
    {
        $this->deliveryReadRepository = $deliveryReadRepository;
        $this->deliveryService = $deliveryService;
        $this->bus = $bus;
    }

    /**
     * @param SelectDelivery $request
     * @return array
     */
    public function handle(SelectDelivery $request)
    {
        $delivery = $this->deliveryReadRepository->getDeliveryService($request->getId());
        if(!$delivery) {
            return [
              'status' => false,
              'error' => 'Invalid delivery service'
            ];
        }
        $products = $this->bus->handle(new GetAllProducts())->mapInto(ProductEntity::class);
        if ($this->deliveryService->isApplicableToCart($products, $delivery)) {
            try {
                $deliveryPrice = $this->deliveryService->getPriceOfDelivery($products, $delivery);
                return [
                    'status' => true,
                    'delivery' => [
                        'name' => $delivery->name,
                        'price' => $deliveryPrice
                    ]
                ];
            } catch (Exception $exception) {
                return [
                    'status' => false,
                    'error' => $exception->getMessage()
                ];
            }

        } else {
            return [
                'status' => false,
                'errors' => [
                    'is-not-applicable-to-cart' => true
                ]
            ];
        }
    }
}

<?php

namespace Deka\Delivery\Application\Services;


use Deka\Cart\Services\Request\GetAllProducts;
use Deka\Delivery\Application\Services\Request\GetDeliveryServices;
use Deka\Delivery\Domain\Interfaces\DeliveryServiceInterface;
use League\Tactician\CommandBus;

/**
 * Class GetDeliveryServicesHandler
 * @package Deka\Delivery\Application\Services
 */
class GetDeliveryServicesHandler
{
    /**
     * @var DeliveryServiceInterface
     */
    private $deliveryService;

    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * GetDeliveryServicesHandler constructor.
     * @param DeliveryServiceInterface $deliveryService
     * @param CommandBus $bus
     */
    public function __construct(
        DeliveryServiceInterface $deliveryService,
        CommandBus $bus
    )
    {
        $this->deliveryService = $deliveryService;
        $this->bus = $bus;
    }

    /**
     * @param GetDeliveryServices $getAllDeliveryServices
     * @return \Illuminate\Support\Collection
     */
    public function handle(GetDeliveryServices $getAllDeliveryServices)
    {
        return $this->deliveryService->getDeliveryServices($this->bus->handle(new GetAllProducts()));
    }
}

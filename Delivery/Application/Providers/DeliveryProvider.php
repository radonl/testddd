<?php

namespace Deka\Delivery\Application\Providers;

use Deka\Common\Application\AbstractServiceProvider;
use Deka\Delivery\Domain\Interfaces\DeliveryServiceInterface;
use Deka\Delivery\Domain\Services\DeliveryService;
use Deka\Delivery\Infrastructure\Interfaces\DeliveryReadRepositoryInterface;
use Deka\Delivery\Infrastructure\Repositories\DeliveryReadRepository;

/**
 * Class DeliveryProvider
 * @package Deka\Delivery\Application\Providers
 */
class DeliveryProvider extends AbstractServiceProvider
{
    public function register()
    {
        $this->app->singleton(DeliveryReadRepositoryInterface::class, DeliveryReadRepository::class);
        $this->app->singleton(DeliveryServiceInterface::class, DeliveryService::class);
    }

    protected $migrationsPath = __DIR__ . '/../../Infrastructure/Migrations';

    protected $routesPath = __DIR__ . '/../../UI/Routes/api.php';
}

<?php

namespace Deka\Delivery\Application\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;


/**
 * Class GetBranchesValidator
 * @package Deka\Delivery\Application\Validation
 */
class GetBranchesValidator extends FormRequest
{
    const RULES = [
        'deliveryServiceId' => 'required|numeric',
        'cityId' => 'required|numeric'
    ];

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($request){
        return Validator::make($request, GetBranchesValidator::RULES);
    }
}

<?php

namespace Deka\Delivery\Application\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;


/**
 * Class SelectDeliveryValidator
 * @package Deka\Delivery\Application\Validation
 */
class SelectDeliveryValidator extends FormRequest
{
    const RULES = [
        'id' => 'required|numeric|min:1'
    ];

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($request){
        return Validator::make($request, SelectDeliveryValidator::RULES);
    }
}

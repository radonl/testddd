<?php

namespace Deka\Delivery\Application\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;


/**
 * Class GetCitiesValidator
 * @package Deka\Delivery\Application\Validation
 */
class GetCitiesValidator extends FormRequest
{
    const RULES = [
        'deliveryServiceId' => 'required|numeric'
    ];

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($request){
        return Validator::make($request, GetCitiesValidator::RULES);
    }
}

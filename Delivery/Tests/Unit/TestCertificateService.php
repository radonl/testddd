<?php

namespace Tests\Unit;

use Deka\Certificate\Infrastructure\Interfaces\CertificateReadRepositoryInterface;
use Deka\Delivery\Infrastructure\Interfaces\DeliveryReadRepositoryInterface;
use Illuminate\Support\Collection;
use League\Tactician\CommandBus;
use Tests\TestCase;

/**
 * Class TestCartService
 * @package Tests\Unit
 */
class TestCertificateService extends TestCase
{
    /**
     * @var \Deka\Delivery\Domain\Services\DeliveryService
     */
    private $service;

    /**
     * @var \Mockery\MockInterface
     */
    private $deliveryReadRepositoryMock;

    /**
     * @var object
     */
    private $delivery;

    /**
     * @var Collection
     */
    private $products;

    /**
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp():void {
        parent::setUp();
        $deliveryReadRepositoryInterface = $this->mock(DeliveryReadRepositoryInterface::class);
        $this->app->bind('Deka\Certificate\Infrastructure\Interfaces\CertificateReadRepositoryInterface', function () use ($deliveryReadRepositoryInterface) {
            return $deliveryReadRepositoryInterface;
        });
        $this->deliveryReadRepositoryMock = $deliveryReadRepositoryInterface;
        $this->service = $this->app->make('\Deka\Delivery\Domain\Services\DeliveryService');
        $this->products = collect([[
            'brandId' => 1,
            'categoryId' => 1
        ]]);
        $this->delivery = new class {
            public $id = 1;
        };
    }

    public function testIsApplicableToCart() {
        $this->deliveryReadRepositoryMock
            ->shouldReceive('brandsIsBlocked')
            ->with([1], 1)
            ->andReturn(true);
        $this->assertFalse($this->service->isApplicableToCart($this->products, $this->delivery));
    }

    /**
     * @throws \Exception
     */
    public function testGetPriceOfDelivery() {
        $price = random_int(1, 100);
        $this->deliveryReadRepositoryMock
            ->shouldReceive('getPriceOfDelivery')
            ->with([1], 1)
            ->andReturn($price);
        $this->assertTrue(
            $this->service->getPriceOfDelivery($this->products, $this->delivery) == $price
        );
    }

    public function testGetCities() {
        $this->deliveryReadRepositoryMock
            ->shouldReceive('getCities')
            ->with($this->delivery->id)
            ->andReturn([]);
        $this->assertTrue($this->service->getCities($this->delivery->id) == []);
    }

    public function testGetBranches() {
        $this->deliveryReadRepositoryMock
            ->shouldReceive('getBranches')
            ->with(1,1)
            ->andReturn([]);
        $this->assertTrue($this->service->getBranches(1,1) == []);
    }

    public function testGetDeliveryServices() {
        $res = collect([]);
        $this->deliveryReadRepositoryMock
            ->shouldReceive('getAvailableDeliveryServices')
            ->with([1])
            ->andReturn($res);
        $this->assertTrue($this->service->getDeliveryServices($this->products)->count() == 0);
    }
}

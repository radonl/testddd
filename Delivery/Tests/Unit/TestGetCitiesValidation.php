<?php

namespace Tests\Unit;

use Deka\Cart\Application\Validation\AddProductRequestValidator;
use Deka\Cart\Application\Validation\RemoveProductRequestValidator;
use Deka\Certificate\Application\Validation\CertificateCheckValidator;
use Deka\Delivery\Application\Validation\GetBranchesValidator;
use Deka\Delivery\Application\Validation\GetCitiesValidator;
use Tests\TestCase;

/**
 * Class TestsUnitAddProductValidation
 * @package Tests\Unit
 */
class TestGetBranchesValidation extends TestCase
{
    /**
     * @var array $rules
     */
    private $rules;

    /**
     * @var \Illuminate\Validation\Validator $validator
     */
    private $validator;

    private $data = [
        'deliveryServiceId' => 1
    ];

    protected function setUp():void {
        parent::setUp();
        $this->validator = app()->get('validator');
        $this->rules = GetCitiesValidator::RULES;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function validationProvider() {
        return [
            'request.success' => [
                'passed' => true,
                'request' => $this->data
            ],
            'deliveryServiceId.validation.required' => [
                'passed' => false,
                'request' => array_merge($this->data, ['deliveryServiceId' => ''])
            ],
            'deliveryServiceId.validation.numeric' => [
                'passed' => false,
                'request' => array_merge($this->data, ['deliveryServiceId' => 'test'])
            ],
            'deliveryServiceId.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data, ['deliveryServiceId' => rand(1,100)])
            ]
        ];
    }

    /**
     * @test
     * @dataProvider validationProvider
     * @param bool $shouldPass
     * @param array $mockedRequestData
     */
    public function validation_results_as_expected($shouldPass, $mockedRequestData)
    {
        $this->assertEquals(
            $shouldPass,
            $this->validate($mockedRequestData)
        );
    }

    /**
     * @param $mockedRequestData
     * @return mixed
     */
    protected function validate($mockedRequestData)
    {
        return $this->validator
            ->make($mockedRequestData, $this->rules)
            ->passes();
    }
}

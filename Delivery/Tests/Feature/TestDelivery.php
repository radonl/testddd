<?php

namespace Tests\Feature;

use App\Domain\Interfaces\CartServiceInterface;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use League\Tactician\CommandBus;
use Symfony\Component\HttpFoundation\Cookie;
use Tests\TestCase;

/**
 * Class TestCrossSale
 * @package Tests\Feature
 */
class TestDelivery extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var \Mockery\MockInterface
     */
    public $busMock;

    protected function setUp():void {
        parent::setUp();
        $bus = $this->mock(CommandBus::class);
        $this->app->bind('League\Tactician\CommandBus', function () use ($bus) {
            return $bus;
        });
        $this->busMock = $bus;
    }

    public function testGetDeliveryServices() {
        $this->busMock->shouldReceive('handle')
            ->andReturn(['test' => 'test']);
        $this->json('GET', '/api/checkout/order/delivery')
            ->assertJson(['test' => 'test']);
    }

    public function testSelectDelivery() {
        $this->busMock->shouldReceive('handle')
            ->andReturn(['test' => 'test']);
        $this->json('GET', '/api/checkout/order/delivery/select/1/')
            ->assertJson(['test' => 'test']);
    }

    public function testGetCities() {
        $this->busMock->shouldReceive('handle')
            ->andReturn(['test' => 'test']);
        $this->json('GET', '/api/checkout/order/delivery-services/1/cities/')
            ->assertJson(['test' => 'test']);
    }

    public function testGetBranches() {
        $this->busMock->shouldReceive('handle')
            ->andReturn(['test' => 'test']);
        $this->json('GET', '/api/checkout/order/delivery-services/1/cities/1/branches/')
            ->assertJson(['test' => 'test']);
    }
}

<?php

namespace Deka\Delivery\Infrastructure\Interfaces;


use Illuminate\Support\Collection;

/**
 * Interface DeliveryReadRepositoryInterface
 * @package Deka\Delivery\Infrastructure\Interfaces
 */
interface DeliveryReadRepositoryInterface
{

    /**
     * @param int $id
     * @return null|object
     */
    public function getDeliveryService(int $id): ?object;

    /**
     * @param array $brands
     * @param int $deliveryId
     * @return bool
     */
    public function brandsIsBlocked(array $brands, int $deliveryId): bool;

    /**
     * @param array $brands
     * @return Collection
     */
    public function getAvailableDeliveryServices(array $brands): Collection;

    /**
     * @param array $categories
     * @param int $deliveryId
     * @return string
     */
    public function getPriceOfDelivery(array $categories, int $deliveryId): string;

    /**
     * @param int $deliveryServiceId
     * @return array
     */
    public function getCities(int $deliveryServiceId): array;

    /**
     * @param int $deliveryServiceId
     * @param int $cityId
     * @return array
     */
    public function getBranches(int $deliveryServiceId, int $cityId): array;
}

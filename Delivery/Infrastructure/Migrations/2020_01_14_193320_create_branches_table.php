<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateBranchesTable
 */
class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkout_branches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('delivery_service_id');
        });

        Schema::table('checkout_branches', function(Blueprint $table) {
            $table->foreign('city_id')->references('id')->on('checkout_cities')->onDelete('cascade');
        });
        Schema::table('checkout_branches', function(Blueprint $table) {
            $table->foreign('delivery_service_id')->references('id')->on('checkout_delivery_service')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checkout_branches', function (Blueprint $table) {
            $table->dropForeign(['city_id']);
        });
        Schema::table('checkout_branches', function (Blueprint $table) {
            $table->dropForeign(['delivery_service_id']);
        });
        Schema::dropIfExists('checkout_branches');
    }
}

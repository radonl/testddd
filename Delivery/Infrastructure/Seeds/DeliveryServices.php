<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class DeliveryServices
 */
class DeliveryServices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('checkout_delivery_service')->insert([
            'sid' => Str::random(10),
            'name' => Str::random(10),
            'description' => Str::random(10)
        ]);

        DB::table('checkout_delivery_service')->insert([
            'sid' => Str::random(10),
            'name' => Str::random(10),
            'description' => Str::random(10)
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class PriceOfDelivery
 */
class PriceOfDelivery extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        DB::table('checkout_price_of_delivery')->insert([
            'delivery_service_id' => 1,
            'price' => random_int(1, 1000),
        ]);

        DB::table('checkout_price_of_delivery')->insert([
            'delivery_service_id' => 1,
            'category_id' => random_int(1,1000),
            'price' => random_int(1, 1000),
        ]);

        DB::table('checkout_price_of_delivery')->insert([
            'delivery_service_id' => 1,
            'category_id' => random_int(1,1000),
            'description' => Str::random(10)
        ]);

        DB::table('checkout_price_of_delivery')->insert([
            'delivery_service_id' => 2,
            'price' => random_int(1, 1000),
        ]);
    }
}

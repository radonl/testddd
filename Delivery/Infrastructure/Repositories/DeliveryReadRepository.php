<?php

namespace Deka\Delivery\Infrastructure\Repositories;

use Deka\Delivery\Infrastructure\Interfaces\DeliveryReadRepositoryInterface;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class DeliveryReadRepository
 * @package Deka\Delivery\Infrastructure\Repositories
 */
class DeliveryReadRepository implements DeliveryReadRepositoryInterface {

    /**
     * @param int $id
     * @return null|object
     */
    public function getDeliveryService(int $id): ?object
    {
        return DB::table('checkout_delivery_service')->where('id', $id)->first();
    }

    /**
     * @param array $brands
     * @param int $deliveryId
     * @return bool
     */
    public function brandsIsBlocked(array $brands, int $deliveryId): bool
    {
        return DB::table('checkout_blocked_delivery_service_for_brand')
                ->whereIn('brand_id', $brands)
                ->where('delivery_service_id', $deliveryId)
                ->count() > 0;
    }

    /**
     * @param array $brands
     * @return Collection
     */
    public function getAvailableDeliveryServices(array $brands): Collection {
        return DB::table('checkout_delivery_service as ds')->whereNotIn('id', function ($q) use ($brands) {
            $q->select('delivery_service_id')->from('checkout_blocked_delivery_service_for_brand')->whereIn('brand_id', $brands);
        })->get();
    }

    /**
     * @param array $categories
     * @param int $deliveryId
     * @return string
     * @throws Exception
     */
    public function getPriceOfDelivery(array $categories, int $deliveryId): string
    {
        $data = DB::table('checkout_price_of_delivery')
            ->whereIn('category_id', $categories)
            ->where('delivery_service_id', $deliveryId)
            ->get();

        if($data->count() == 0) {
            $priceOfDelivery =  DB::table('checkout_price_of_delivery')
                ->where('delivery_service_id', $deliveryId)
                ->whereNull('category_id')
                ->first();
            if(!$priceOfDelivery) {
                throw new Exception('Delivery has no default price');
            }
            if($priceOfDelivery->price || $priceOfDelivery->description) {
                return $priceOfDelivery->description ? $priceOfDelivery->description : $priceOfDelivery->price;
            } else {
                throw new Exception('Delivery price is incorrect');
            }
        } else if($data->where('description', '!=', null)->count() > 0) {
            return $data->pluck('description')->unique()->implode(', ');
        } else {
            return $data->max('price');
        }
    }

    /**
     * @param int $deliveryServiceId
     * @return array
     */
    public function getCities(int $deliveryServiceId): array
    {
        return DB::table('checkout_delivery_service_city as dsc')
            ->where('dsc.delivery_service_id', $deliveryServiceId)
            ->leftJoin('checkout_cities as c', 'dsc.city_id','=', 'c.id')
            ->get(['c.id', 'c.name'])
            ->toArray();
    }

    /**
     * @param int $deliveryServiceId
     * @param int $cityId
     * @return array
     */
    public function getBranches(int $deliveryServiceId, int $cityId): array
    {
        return DB::table('checkout_branches')
            ->where('delivery_service_id', $deliveryServiceId)
            ->where('city_id', $cityId)
            ->get(['id', 'name'])
            ->toArray();
    }
}

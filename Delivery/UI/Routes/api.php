<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->middleware('api')
    ->namespace("Deka\Delivery\UI\Controllers")
    ->group(function () {
        Route::get('/checkout/order/delivery/', "DeliveryController@getDeliveryServices");
        Route::get('/checkout/order/delivery/select/{id}/', "DeliveryController@selectDelivery");
        Route::get('/checkout/order/delivery-services/{deliveryServiceId}/cities/', "DeliveryController@getCities");
        Route::get('/checkout/order/delivery-services/{deliveryServiceId}/cities/{cityId}/branches/', "DeliveryController@getBranches");
    });

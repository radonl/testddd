<?php

namespace Deka\Delivery\UI\Controllers;


use Deka\Delivery\Application\Services\Request\GetBranches;
use Deka\Delivery\Application\Services\Request\GetCities;
use Deka\Delivery\Application\Services\Request\GetDeliveryServices;
use Deka\Delivery\Application\Services\Request\SelectDelivery;
use Illuminate\Routing\Controller;
use League\Tactician\CommandBus;

/**
 * Class DeliveryController
 * @package Deka\Delivery\UI\Controllers
 */
class DeliveryController extends Controller
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CommentsController constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus) {
        $this->bus = $bus;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDeliveryServices() {
        return response()->json($this->bus->handle(new GetDeliveryServices()), 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function selectDelivery($id) {
        $request = new SelectDelivery($id);
        if($request->getValidator()->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $request->getValidator()->errors()
            ], 422);
        } else {
            return response()->json($this->bus->handle($request), 200);
        }
    }

    /**
     * @param $deliveryServiceId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCities($deliveryServiceId) {
        $request = new GetCities($deliveryServiceId);
        if($request->getValidator()->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $request->getValidator()->errors()
            ], 422);
        } else {
            return response()->json($this->bus->handle($request), 200);
        }
    }

    /**
     * @param $deliveryServiceId
     * @param $cityId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBranches($deliveryServiceId, $cityId) {
        $request = new GetBranches($deliveryServiceId, $cityId);
        if($request->getValidator()->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $request->getValidator()->errors()
            ]);
        } else {
            return response()->json($this->bus->handle($request), 200);
        }
    }
}

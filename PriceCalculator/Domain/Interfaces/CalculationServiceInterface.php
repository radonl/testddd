<?php

namespace Deka\PriceCalculator\Domain\Interfaces;

/**
 * Interface CalculationServiceInterface
 * @package Deka\PriceCalculator\Domain\Interfaces
 */
interface CalculationServiceInterface {
    /**
     * @param array $products
     * @param array $userDiscount
     * @param array $promoCode
     * @return array
     */
    public function calculatePrice(array $products, array $userDiscount, array $promoCode) : array ;
}

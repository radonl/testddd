<?php

namespace Deka\PriceCalculator\Domain\Services;

use Deka\PriceCalculator\Domain\Interfaces\CalculationServiceInterface;

/**
 * Class CalculationService
 * @package Deka\PriceCalculator\Domain\Services
 */
class CalculationService implements CalculationServiceInterface {

    /**
     * @param array $products
     * @return array
     */
    private function getSummary(array $products): array {
        $price = 0;
        $old_price = 0;
        $count = 0;
        foreach ($products as $product) {
            $price += $product['price'] * $product['count'];
            $old_price += $product['old_price']  * $product['count'];
            $count += $product['count'];
        }
        return [
            'discount_percent' => round($price > 0 ? (100 - ($price / $old_price * 100)) : 0 , 2),
            'count' => $count,
            'total' => $price
        ];
    }

    /**
     * @param array $products
     * @param array $promoCode
     * @return array
     */
    private function applyPromoCode(array $products, array $promoCode): array {
        return collect($products)->map(function ($item, $key) use ($promoCode) {
            if ((
                $key == $promoCode['product_id'] ||
                $item['brandId'] == $promoCode['brand_id'] ||
                $item['categoryId'] == $promoCode['category_id']) &&
                $item['discount_percent'] < $promoCode['percent']
            ) {
                $item['price'] = $this->decrementPercent($item['old_price'], $promoCode['percent']);
                $item['discount_percent'] = $promoCode['percent'];
            }
            return $item;
        })->toArray();
    }

    /**
     * @param array $products
     * @param int $percent
     * @return array
     */
    private function applyUserDiscount(array $products, int $percent): array {
        return collect($products)->map(function ($item) use ($percent) {
           if($item['discount_percent'] < $percent) {
               $item['price'] = $this->decrementPercent($item['old_price'], $percent);
               $item['discount_percent'] = $percent;
           }
           return $item;
        })->toArray();
    }

    /**
     * @param $price
     * @param $percent
     * @return int
     */
    private function decrementPercent($price, $percent): int {
        return $price - ($price * $percent / 100);
    }

    /**
     * @param array $products
     * @param array $userDiscount
     * @param array $promoCode
     * @return array
     */
    public function calculatePrice(array $products, array $userDiscount, array $promoCode): array
    {
        if ($userDiscount) {
            $products = $this->applyUserDiscount($products, $userDiscount['json']['user']['discount']);
        }

        if ($promoCode) {
            $products = $this->applyPromoCode($products, $promoCode);
        }

        return [
            'items' => $products,
            'info' => $this->getSummary($products)
        ];
    }
}

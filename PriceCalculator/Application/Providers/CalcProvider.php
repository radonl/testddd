<?php

namespace Deka\PriceCalculator\Application\Providers;

use Deka\PriceCalculator\Domain\Interfaces\CalculationServiceInterface;
use Deka\PriceCalculator\Domain\Services\CalculationService;
use Deka\Common\Application\AbstractServiceProvider;

/**
 * Class CalcProvider
 * @package Deka\PriceCalculator\Application\Providers
 */

/**
 * Class CalcProvider
 * @package Deka\PriceCalculator\Application\Providers
 */
class CalcProvider extends AbstractServiceProvider
{
    public function register()
    {
        $this->app->singleton(CalculationServiceInterface::class, CalculationService::class);
    }

    protected $migrationsPath = __DIR__ . '/../../Infrastructure/Migrations';

    protected $routesPath = __DIR__ . '/../../UI/Routes/api.php';
}

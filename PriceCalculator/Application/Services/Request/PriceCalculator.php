<?php

namespace Deka\PriceCalculator\Application\Services\Request;

/**
 * Class PriceCalculator
 * @package Deka\PriceCalculator\Application\Services\Request
 */
class  PriceCalculator {

    /**
     * @var array
     */
    private $products;

    /**
     * @var array
     */
    private $userDiscount;

    /**
     * @var array
     */
    private $promoCode;

    /**
     * PriceCalculator constructor.
     * @param array $products
     * @param array $userDiscount
     * @param $promoCode
     */
    public function __construct(array $products = [], array $userDiscount = [], array $promoCode = [])
    {
       $this->products = $products;
       $this->userDiscount = $userDiscount;
       $this->promoCode = $promoCode;
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @return array
     */
    public function getUserDiscount(): array
    {
        return $this->userDiscount;
    }

    /**
     * @return array
     */
    public function getPromoCode(): array
    {
        return $this->promoCode;
    }
}

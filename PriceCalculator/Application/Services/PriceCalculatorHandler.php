<?php

namespace Deka\PriceCalculator\Application\Services;

use Deka\PriceCalculator\Application\Services\Request\PriceCalculator;
use Deka\PriceCalculator\Domain\Interfaces\CalculationServiceInterface;

/**
 * Class PriceCalculatorHandler
 * @package Deka\PriceCalculator\Application\Services
 */
class PriceCalculatorHandler
{
    /**
     * @var CalculationServiceInterface
     */
    private $calculationService;

    /**
     * PriceCalculatorHandler constructor.
     * @param CalculationServiceInterface $calculationService
     */
    public function __construct(CalculationServiceInterface $calculationService)
    {
        $this->calculationService = $calculationService;
    }

    /**
     * @param PriceCalculator $request
     * @return array
     */
    public function handle(PriceCalculator $request) {
        return $this->calculationService->calculatePrice($request->getProducts(), $request->getUserDiscount(), $request->getPromoCode());
    }
}

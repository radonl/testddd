<?php

namespace Tests\Unit;

use Deka\PriceCalculator\Domain\Interfaces\CalculationServiceInterface;
use Tests\TestCase;

/**
 * Class TestCartService
 * @package Tests\Unit
 */
class TestCalculationService extends TestCase
{
    /**
     * @var CalculationServiceInterface
     */
    private $service;

    /**
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp():void {
        parent::setUp();
        $this->service = $this->app->make('\Deka\PriceCalculator\Domain\Services\CalculationService');
    }

    public function testCreateOrder() {
        $this->assertTrue($this->service->calculatePrice([], [], [])['info']['total'] == 0);
    }
}

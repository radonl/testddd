<?php

namespace Deka\Payment\Domain\Interfaces;

use Illuminate\Support\Collection;

/**
 * Interface PaymentMethodsServiceInterface
 * @package Deka\Payment\Domain\Interfaces
 */
interface PaymentMethodsServiceInterface {
    /**
     * @param int $paymentMethodId
     * @return bool
     */
    public function isApplicableToCart(int $paymentMethodId): bool;

    /**
     * @return Collection
     */
    public function getPaymentMethodsForBrands(): Collection;

    /**
     * @param int $paymentMethodId
     * @return bool
     */
    public function paymentMethodIsExist(int $paymentMethodId): bool;
}

<?php

namespace Deka\Payment\Domain\Services;

use Deka\Cart\Services\Request\GetAllProducts;
use Deka\Payment\Application\Infrastructure\Interfaces\PaymentMethodsReadRepositoryInterface;
use Deka\Payment\Domain\Interfaces\PaymentMethodsServiceInterface;
use Illuminate\Support\Collection;
use League\Tactician\CommandBus;

/**
 * Class PaymentMethodsService
 * @package Deka\Payment\Domain\Services
 */
class PaymentMethodsService implements PaymentMethodsServiceInterface {

    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * @var PaymentMethodsReadRepositoryInterface
     */
    private $paymentMethodsReadRepository;

    /**
     * PaymentMethodsService constructor.
     * @param CommandBus $bus
     * @param PaymentMethodsReadRepositoryInterface $paymentMethodsReadRepository
     */
    public function __construct(
        CommandBus $bus,
        PaymentMethodsReadRepositoryInterface $paymentMethodsReadRepository
    )
    {
        $this->bus = $bus;
        $this->paymentMethodsReadRepository = $paymentMethodsReadRepository;
    }


    /**
     * @param int $paymentMethodId
     * @return bool
     */
    public function isApplicableToCart(int $paymentMethodId): bool
    {
        return !$this->paymentMethodsReadRepository->brandsIsBlocked(
            collect($this->bus->handle(new GetAllProducts())->toArray())->pluck('brandId'),
            $paymentMethodId
        );
    }

    /**
     * @return Collection
     */
    public function getPaymentMethodsForBrands(): Collection {
        return $this->paymentMethodsReadRepository->getPaymentMethodsForBrands(
            collect($this->bus->handle(new GetAllProducts())->toArray())->pluck('brandId')
        );
    }

    /**
     * @param int $paymentMethodId
     * @return bool
     */
    public function paymentMethodIsExist(int $paymentMethodId): bool {
        return $this->paymentMethodsReadRepository->paymentMethodIsExist($paymentMethodId);
    }
}

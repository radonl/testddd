<?php

namespace Deka\Payment\Application\Infrastructure\Interfaces;


use Illuminate\Support\Collection;

/**
 * Interface PaymentMethodsReadRepositoryInterface
 * @package Deka\Payment\Application\Infrastructure\Interfaces
 */
interface PaymentMethodsReadRepositoryInterface
{
    /**
     * @param Collection $brands
     * @return Collection
     */
    public function getPaymentMethodsForBrands(Collection $brands): Collection;

    /**
     * @param Collection $brands
     * @param int $paymentMethodId
     * @return bool
     */
    public function brandsIsBlocked(Collection $brands, int $paymentMethodId): bool;

    /**
     * @param int $paymentMethodId
     * @return bool
     */
    public function paymentMethodIsExist(int $paymentMethodId): bool;
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreatePaymentMethodsTable
 */
class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkout_payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sid');
            $table->string('name');
            $table->string('description');
        });
        Schema::table('checkout_orders', function (Blueprint $table) {
            $table->unsignedBigInteger('payment_method_id')->nullable();
        });
        Schema::table('checkout_orders', function(Blueprint $table) {
            $table->foreign('payment_method_id')->references('id')->on('checkout_payment_methods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checkout_orders', function(Blueprint $table) {
            $table->dropForeign(['payment_method_id']);
        });
        Schema::table('checkout_orders', function (Blueprint $table) {
            $table->dropColumn('payment_method_id');
        });
        Schema::dropIfExists('checkout_payment_methods');
    }
}

<?php

namespace Deka\Payment\Application\Infrastructure\Repositories;


use Deka\Payment\Application\Infrastructure\Interfaces\PaymentMethodsReadRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class PaymentMethodsReadRepository
 * @package Deka\Payment\Application\Infrastructure\Repositories
 */
class PaymentMethodsReadRepository implements PaymentMethodsReadRepositoryInterface {

    /**
     * @param Collection $brands
     * @return Collection
     */
    public function getPaymentMethodsForBrands(Collection $brands): Collection
    {
        return  DB::table('checkout_payment_methods')->whereNotIn('id', function ($q) use ($brands) {
            $q->select('payment_method_id')->from('checkout_blocked_payment_methods_for_brand')->whereIn('brand_id', $brands);
        })->get();
    }

    /**
     * @param Collection $brands
     * @param int $paymentMethodId
     * @return bool
     */
    public function brandsIsBlocked(Collection $brands, int $paymentMethodId): bool
    {
        return DB::table('checkout_blocked_payment_methods_for_brand')
                ->whereIn('brand_id', $brands)
                ->where('payment_method_id', $paymentMethodId)
                ->count() > 0;
    }

    /**
     * @param int $paymentMethodId
     * @return bool
     */
    public function paymentMethodIsExist(int $paymentMethodId): bool
    {
        return (bool) DB::table('checkout_payment_methods')
            ->find($paymentMethodId);
    }
}

<?php

namespace Tests\Unit;

use Deka\Payment\Application\Infrastructure\Interfaces\PaymentMethodsReadRepositoryInterface;
use Deka\Payment\Domain\Interfaces\PaymentMethodsServiceInterface;
use League\Tactician\CommandBus;
use Tests\TestCase;

/**
 * Class TestCartService
 * @package Tests\Unit
 */
class TestPaymentMethods extends TestCase
{
    /**
     * @var PaymentMethodsServiceInterface
     */
    private $service;

    /**
     * @var \Mockery\MockInterface
     */
    private $paymentMethodsReadRepositoryMock;

    /**
     * @var \Mockery\MockInterface
     */
    private $busMock;

    /**
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp():void {
        parent::setUp();
        $paymentMethodsReadRepositoryInterface = $this->mock(PaymentMethodsReadRepositoryInterface::class);
        $this->app->bind('Deka\Payment\Application\Infrastructure\Interfaces\PaymentMethodsReadRepositoryInterface', function () use ($paymentMethodsReadRepositoryInterface) {
            return $paymentMethodsReadRepositoryInterface;
        });
        $this->paymentMethodsReadRepositoryMock = $paymentMethodsReadRepositoryInterface;
        $busMock = $this->mock(CommandBus::class);
        $this->app->bind('League\Tactician\CommandBus', function () use ($busMock) {
            return $busMock;
        });
        $this->busMock = $busMock;
        $this->service = $this->app->make('\Deka\Payment\Domain\Services\PaymentMethodsService');
    }

    public function testIsApplicableToCart() {
        $this->busMock
            ->shouldReceive('handle')
            ->andReturn(collect(['brandId' => 1]));
        $this->paymentMethodsReadRepositoryMock
            ->shouldReceive('brandsIsBlocked')
            ->andReturn(false);
        $this->assertTrue($this->service->isApplicableToCart(1));
    }

    public function testGetPaymentMethodsForBrands() {
        $this->busMock
            ->shouldReceive('handle')
            ->andReturn(collect(['brandId' => 1]));
        $this->paymentMethodsReadRepositoryMock
            ->shouldReceive('getPaymentMethodsForBrands')
            ->andReturn(collect([]));
        $this->assertTrue($this->service->getPaymentMethodsForBrands()->count() == 0);
    }

    public function testPaymentMethodIsExist() {
        $this->paymentMethodsReadRepositoryMock
            ->shouldReceive('paymentMethodIsExist')
            ->andReturn(false);
        $this->assertFalse($this->service->paymentMethodIsExist(1));
    }
}

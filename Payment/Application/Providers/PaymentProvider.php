<?php

namespace Deka\Payment\Application\Providers;

use Deka\Common\Application\AbstractServiceProvider;
use Deka\Payment\Application\Infrastructure\Interfaces\PaymentMethodsReadRepositoryInterface;
use Deka\Payment\Application\Infrastructure\Repositories\PaymentMethodsReadRepository;
use Deka\Payment\Domain\Interfaces\PaymentMethodsServiceInterface;
use Deka\Payment\Domain\Services\PaymentMethodsService;

/**
 * Class PaymentProvider
 * @package Deka\Payment\Application\Providers
 */
class PaymentProvider extends AbstractServiceProvider
{
    public function register()
    {
        $this->app->singleton(PaymentMethodsReadRepositoryInterface::class, PaymentMethodsReadRepository::class);
        $this->app->singleton(PaymentMethodsServiceInterface::class, PaymentMethodsService::class);
    }

    protected $migrationsPath = __DIR__ . '/../../Infrastructure/Migrations';

    protected $routesPath = __DIR__ . '/../../UI/Routes/api.php';
}

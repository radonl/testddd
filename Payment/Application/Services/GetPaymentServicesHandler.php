<?php

namespace Deka\Payment\Application\Services;

use Deka\Cart\Services\Request\GetAllProducts;
use Deka\Payment\Application\Infrastructure\Interfaces\PaymentMethodsReadRepositoryInterface;
use Deka\Payment\Application\Services\Request\GetPaymentServices;
use Deka\Payment\Domain\Interfaces\PaymentMethodsServiceInterface;
use League\Tactician\CommandBus;

/**
 * Class GetPaymentServicesHandler
 * @package Deka\Payment\Application\Services
 */
class GetPaymentServicesHandler
{
    /**
     * @var PaymentMethodsServiceInterface
     */
    private $paymentMethodsService;

    /**
     * GetPaymentServicesHandler constructor.
     * @param PaymentMethodsServiceInterface $paymentMethodsService
     */
    public function __construct(PaymentMethodsServiceInterface $paymentMethodsService)
    {
        $this->paymentMethodsService = $paymentMethodsService;
    }

    /**
     * @param GetPaymentServices $request
     * @return \Illuminate\Support\Collection
     */
    public function handle(GetPaymentServices $request) {
        return $this->paymentMethodsService->getPaymentMethodsForBrands();
    }
}

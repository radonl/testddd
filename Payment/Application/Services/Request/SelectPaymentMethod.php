<?php

namespace Deka\Payment\Application\Services\Request;

/**
 * Class GetPaymentServices
 * @package Deka\Payment\Application\Services\Request
 */
class SelectPaymentMethod
{
    /**
     * @var int
     */
    private $id;

    /**
     * GetAllDeliveryServices constructor.
     * @param int $id
     */
    public function __construct(int $id){
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}

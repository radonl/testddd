<?php

namespace Deka\Payment\Application\Services;

use Deka\Cart\Services\Request\GetAllProducts;
use Deka\Payment\Application\Infrastructure\Interfaces\PaymentMethodsReadRepositoryInterface;
use Deka\Payment\Application\Services\Request\GetPaymentServices;
use Deka\Payment\Application\Services\Request\SelectPaymentMethod;
use Deka\Payment\Domain\Interfaces\PaymentMethodsServiceInterface;
use League\Tactician\CommandBus;

/**
 * Class GetPaymentServicesHandler
 * @package Deka\Payment\Application\Services
 */
class SelectPaymentMethodHandler
{
    /**
     * @var PaymentMethodsServiceInterface
     */
    private $paymentMethodsService;

    /**
     * SelectPaymentMethodHandler constructor.
     * @param PaymentMethodsServiceInterface $paymentMethodsService
     */
    public function __construct(PaymentMethodsServiceInterface $paymentMethodsService)
    {
        $this->paymentMethodsService = $paymentMethodsService;
    }

    /**
     * @param SelectPaymentMethod $request
     * @return array
     */
    public function handle(SelectPaymentMethod $request) {
        if ($this->paymentMethodsService->paymentMethodIsExist($request->getId())) {
            if ($this->paymentMethodsService->isApplicableToCart($request->getId())) {
                return [
                    'status' => true
                ];
            } else {
                return [
                    'status' => false,
                    'error' => 'Is not applicable to cart'
                ];
            }
        } else {
            return [
                'status' => 'false',
                'error' => 'Payment method does not exist'
            ];
        }
    }
}

<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->middleware('api')
    ->namespace("Deka\Payment\UI\Controllers")
    ->group(function () {
        Route::get('/checkout/order/payment/', "OrderController@payment");
    });

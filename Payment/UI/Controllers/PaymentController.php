<?php

namespace Deka\Payment\UI\Controllers;

use Deka\Payment\Application\Services\Request\GetPaymentServices;
use Illuminate\Routing\Controller;
use League\Tactician\CommandBus;

/**
 * Class PaymentController
 * @package Deka\Payment\UI\Controllers
 */
class PaymentController extends Controller
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CommentsController constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus) {
        $this->bus = $bus;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function payment() {
        return response()->json($this->bus->handle(new GetPaymentServices()), 200);
    }
}

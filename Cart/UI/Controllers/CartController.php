<?php

namespace Deka\Cart\UI\Controllers;

use Deka\Cart\Services\Request\AddProduct;
use Deka\Cart\Services\Request\GetFullInformation;
use Deka\Cart\Services\Request\GetProductsCount;
use Deka\Cart\Services\Request\RemoveProduct;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Cookie;
use League\Tactician\CommandBus;

/**
 * Class CartController
 * @package Deka\Cart\UI\Controllers
 */
class CartController extends Controller
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CommentsController constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus) {
        $this->bus = $bus;
    }

    /**
     * @param $id
     * @param int $qty
     * @return \Illuminate\Http\JsonResponse
     */
    public function add($id, $qty = 1) {
        $request = new AddProduct($id, $qty);
        if($request->getValidator()->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $request->getValidator()->errors()
            ], 422);
        } else {
            $response = response()->json($this->bus->handle($request), 200);
            foreach (Cookie::getQueuedCookies() as $cookie) {
                $response->withCookie($cookie);
            }
            return $response;
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove($id) {
        $request = new RemoveProduct($id);
        if($request->getValidator()->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $request->getValidator()->errors()
            ], 422);
        } else {
            $removeProductResult = $this->bus->handle($request);
            if($removeProductResult['status']) {
                $response = response()->json($removeProductResult['response'], 200);
                foreach (Cookie::getQueuedCookies() as $cookie) {
                    $response->withCookie($cookie);
                }
                return $response;
            } else {
                return response()->json($removeProductResult, 422);
            }
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll() {
        return response()->json($this->bus->handle(new GetFullInformation()),200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCount() {
        return response()->json($this->bus->handle(new GetProductsCount()), 200);
    }
}

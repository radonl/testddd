<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->middleware('api')
    ->namespace("Deka\Cart\UI\Controllers")
    ->group(function () {
        Route::get('/checkout/cart/product/add/{id}/{qty?}/', "CartController@add");
        Route::get('/checkout/cart/product/remove/{id}/', "CartController@remove");
        Route::get('/checkout/cart/', "CartController@getAll");
        Route::get('/checkout/cart/info/count/', "CartController@getCount");
    });

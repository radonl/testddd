<?php

namespace Deka\Cart\Events;

use Deka\Cart\Domain\Entity\ProductEntity;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class RemoveProductEvent
 * @package Deka\Cart\Events
 */
class RemoveProductEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var ProductEntity
     */
    public $productEntity;

    /**
     * Create a new event instance.
     *
     * @param ProductEntity $productEntity
     */
    public function __construct(ProductEntity $productEntity)
    {
        $this->productEntity = $productEntity;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        //ToDo Must be updated as required
        return new PrivateChannel('channel-name');
    }
}

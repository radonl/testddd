<?php

namespace Deka\Cart\Application\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;


/**
 * Class AddProductRequestValidator
 * @package Deka\Cart\Application\Validation
 */

/**
 * Class AddProductRequestValidator
 * @package Deka\Cart\Application\Validation
 */
class AddProductRequestValidator extends FormRequest
{
    const RULES = [
        'g_id' => 'required|numeric',
        'quantity' => 'required|numeric|min:1'
    ];

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($request){
        return Validator::make($request, AddProductRequestValidator::RULES);
    }
}

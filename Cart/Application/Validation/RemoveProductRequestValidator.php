<?php

namespace Deka\Cart\Application\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;


/**
 * Class RemoveProductRequestValidator
 * @package Deka\Cart\Application\Validation
 */
class RemoveProductRequestValidator extends FormRequest
{
    const RULES = [
        'g_id' => 'required|numeric:min:1'
    ];

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($request){
        return Validator::make($request, RemoveProductRequestValidator::RULES);
    }
}

<?php

namespace Deka\Cart\Application\Providers;

use Deka\Cart\Domain\Entity\CartEntity;
use Deka\Cart\Domain\Interfaces\CartEntityInterface;
use Deka\Cart\Domain\Interfaces\CartServiceInterface;
use Deka\Cart\Domain\Services\CartService;
use Deka\Cart\Infrastructure\Interfaces\CartRepositoryInterface;
use Deka\Cart\Infrastructure\Repositories\CartRepository;
use Deka\Common\Application\AbstractServiceProvider;

/**
 * Class CartProvider
 * @package Deka\Cart\Application\Providers
 */
class CartProvider extends AbstractServiceProvider
{
    public function register()
    {
        $this->app->singleton(CartServiceInterface::class, CartService::class);
        $this->app->singleton(CartEntityInterface::class, CartEntity::class);
        $this->app->singleton(CartRepositoryInterface::class, CartRepository::class);
    }

    protected $migrationsPath = __DIR__ . '/../../Infrastructure/Migrations';

    protected $routesPath = __DIR__ . '/../../UI/Routes/api.php';
}

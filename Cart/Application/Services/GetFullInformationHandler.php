<?php

namespace Deka\Cart\Services;

use Deka\PriceCalculator\Application\Services\Request\PriceCalculator;
use Deka\Cart\Domain\Interfaces\CartServiceInterface;
use Deka\Cart\Services\Request\GetFullInformation;
use League\Tactician\CommandBus;

/**
 * Class GetFullInformationHandler
 * @package Deka\Cart\Services
 */
class GetFullInformationHandler
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * @var CartServiceInterface
     */
    private $cartService;

    /**
     * GetFullInformationHandler constructor.
     * @param CommandBus $bus
     * @param CartServiceInterface $cartService
     */
    public function __construct(CommandBus $bus, CartServiceInterface $cartService)
    {
        $this->bus = $bus;
        $this->cartService = $cartService;
    }

    /**
     * @param GetFullInformation $request
     * @return mixed
     */
    public function handle(GetFullInformation $request)
    {
        $cart = $this->cartService->getCart();
        return $this->bus->handle(new PriceCalculator($cart->getProducts()->toArray()));
    }
}

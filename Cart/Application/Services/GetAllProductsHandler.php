<?php

namespace Deka\Cart\Services;

use Deka\Cart\Domain\Interfaces\CartServiceInterface;
use Deka\Cart\Services\Request\GetAllProducts;

/**
 * Class GetAllProductsHandler
 * @package Deka\Cart\Services
 */
class GetAllProductsHandler
{

    /**
     * @var CartServiceInterface
     */
    private $cardService;

    /**
     * GetAllProductsHandler constructor.
     * @param CartServiceInterface $card_service
     */
    public function __construct(CartServiceInterface $card_service)
    {
        $this->cardService = $card_service;
    }

    /**
     * @param GetAllProducts $request
     * @return \Illuminate\Support\Collection
     */
    public function handle(GetAllProducts $request)
    {
        return $this->cardService->getCart()->getProducts();
    }
}

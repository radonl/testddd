<?php

namespace Deka\Cart\Services;

use Deka\Cart\Domain\Interfaces\CartServiceInterface;
use Deka\Cart\Services\Request\GetCart;

/**
 * Class GetCartHandler
 * @package Deka\Cart\Services
 */
class GetCartHandler
{
    /**
     * @var CartServiceInterface
     */
    private $cartService;

    /**
     * GetCartHandler constructor.
     * @param CartServiceInterface $cartService
     */
    public function __construct(CartServiceInterface $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * @param GetCart $getCart
     * @return array
     */
    public function handle(GetCart $getCart)
    {
        return $this->cartService->getCart()->toArray();
    }
}

<?php

namespace Deka\Cart\Services;

use Deka\Cart\Domain\Interfaces\CartServiceInterface;
use Deka\Cart\Services\Request\GetProductsCount;
use League\Tactician\CommandBus;

/**
 * Class GetProductsCountHandler
 * @package Deka\Cart\Services
 */
class GetProductsCountHandler
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * @var CartServiceInterface
     */
    private $cartService;

    /**
     * GetProductsCountHandler constructor.
     * @param CommandBus $bus
     * @param CartServiceInterface $cartService
     */
    public function __construct(CommandBus $bus, CartServiceInterface $cartService)
    {
        $this->bus = $bus;
        $this->cartService = $cartService;
    }

    /**
     * @param GetProductsCount $request
     * @return array
     */
    public function handle(GetProductsCount $request)
    {
        return [
            'count' => $this->cartService->getProductsCount()
        ];
    }
}

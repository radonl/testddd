<?php

namespace Deka\Cart\Services\Request;

use Deka\Cart\Application\Validation\RemoveProductRequestValidator;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class RemoveProduct
 * @package Deka\Cart\Services\Request
 */
class RemoveProduct {

    /**
     * @var mixed
     */
    private $g_id;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * RemoveProduct constructor.
     * @param $g_id
     */
    public function __construct($g_id)
    {
        $this->g_id = $g_id;
        $this->validator = RemoveProductRequestValidator::make($this->toArray());
    }

    /**
     * @return int
     */
    public function getGId(): int
    {
        return $this->g_id;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        return [
          'g_id' => $this->g_id
        ];
    }

    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }
}

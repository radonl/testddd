<?php
namespace Deka\Cart\Services\Request;

use Deka\Cart\Application\Validation\AddProductRequestValidator;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class AddProduct
 * @package Deka\Cart\Services\Request
 */
class AddProduct {

    /**
     * @var int
     */
    private $g_id;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * AddProduct constructor.
     * @param int $g_id
     * @param int $quantity
     */
    public function __construct(int $g_id, int $quantity)
    {
        $this->g_id = $g_id;
        $this->quantity = $quantity;
        $this->validator = AddProductRequestValidator::make([
            'g_id' => $this->g_id,
            'quantity' => $this->quantity
        ]);
    }

    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }

    /**
     * @return int
     */
    public function getGId(): int
    {
        return $this->g_id;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}

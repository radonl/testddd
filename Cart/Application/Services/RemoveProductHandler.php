<?php

namespace Deka\Cart\Services;


use Deka\PriceCalculator\Application\Services\Request\PriceCalculator;
use Deka\Cart\Domain\Interfaces\CartServiceInterface;
use Deka\Cart\Services\Request\RemoveProduct;
use Exception;
use League\Tactician\CommandBus;

/**
 * Class RemoveProductHandler
 * @package Deka\Cart\Services
 */
class RemoveProductHandler
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * @var CartServiceInterface
     */
    private $cartService;

    /**
     * RemoveProductHandler constructor.
     * @param CommandBus $bus
     * @param CartServiceInterface $cartService
     */
    public function __construct(
        CommandBus $bus,
        CartServiceInterface $cartService
    )
    {
        $this->bus = $bus;
        $this->cartService = $cartService;
    }

    /**
     * @param RemoveProduct $request
     * @return array
     */
    public function handle(RemoveProduct $request)
    {
        try {
            $this->cartService->remove($request->getGId());
            return [
                'status' => true,
                'response' => [
                    'info' => $this->bus->handle(new PriceCalculator($this->cartService->getCart()->getProducts()->toArray()))
                ]
            ];
        } catch (Exception $exception) {
            return [
                'status' => false,
                'errors' => [
                    $exception->getMessage()
                ]
            ];
        }
    }
}

<?php

namespace Deka\Cart\Services;

use Deka\PriceCalculator\Application\Services\Request\PriceCalculator;
use Deka\Cart\Domain\Interfaces\CartServiceInterface;
use Deka\Cart\Services\Request\AddProduct;
use Exception;
use League\Tactician\CommandBus;

/**
 * Class AddProductHandler
 * @package Deka\Cart\Services
 */
class AddProductHandler
{

    /**
     * @var CartServiceInterface
     */
    private $cardService;

    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * AddProductHandler constructor.
     * @param CartServiceInterface $card_service
     * @param CommandBus $bus
     */
    public function __construct(
        CartServiceInterface $card_service,
        CommandBus $bus
    )
    {
        $this->cardService = $card_service;
        $this->bus = $bus;
    }

    /**
     * @param AddProduct $request
     * @return array
     */
    public function handle(AddProduct $request)
    {
        try {
            $item = $this->cardService->add($request->getGId(), $request->getQuantity());
            return [
                'item' => $item->toArray(),
                'info' => $this->bus->handle(new PriceCalculator($this->cardService->getCart()->getProducts()->toArray()))
            ];
        } catch (Exception $exception) {
            return [
                'errors' => [
                    $exception->getMessage()
                ]
            ];
        }
    }
}

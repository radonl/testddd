<?php

namespace Tests\Feature;

use App\Domain\Interfaces\CartServiceInterface;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use League\Tactician\CommandBus;
use Symfony\Component\HttpFoundation\Cookie;
use Tests\TestCase;

class TestCart extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var \Mockery\MockInterface
     */
    public $busMock;

    protected function setUp():void {
        parent::setUp();
        $bus = $this->mock(CommandBus::class);
        $this->app->bind('League\Tactician\CommandBus', function () use ($bus) {
            return $bus;
        });
        $this->busMock = $bus;
    }

    public function testAddProduct() {
        $this->busMock->shouldReceive('handle')
            ->andReturn(['test' => 'test']);
        $this->json('GET', '/api/checkout/cart/product/add/1/1/')
            ->assertJson([
                'test' => 'test'
            ]);

    }

    public function testRemoveProduct() {
        $this->busMock->shouldReceive('handle')
            ->andReturn([
                'status' => true,
                'response' => [
                    'test' => 'test'
                ]
            ]);
        $this->json('GET', '/api/checkout/cart/product/remove/1/')
            ->assertJson([
                'test' => 'test'
            ]);
    }

    public function testCart() {
        $this->busMock->shouldReceive('handle')
            ->andReturn(['test' => 'test']);
        $this->json('GET', '/api/checkout/cart/')->assertJson([
            'test' => 'test'
        ]);
    }

    public function testCount() {
        $this->busMock->shouldReceive('handle')
            ->andReturn(['test' => 'test']);
        $this->json('GET', '/api/checkout/cart/info/count/')->assertJson([
            'test' => 'test'
        ]);
    }
}

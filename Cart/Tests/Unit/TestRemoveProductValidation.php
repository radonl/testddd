<?php

namespace Tests\Unit;

use Deka\Cart\Application\Validation\AddProductRequestValidator;
use Deka\Cart\Application\Validation\RemoveProductRequestValidator;
use Tests\TestCase;

/**
 * Class TestsUnitAddProductValidation
 * @package Tests\Unit
 */
class TestRemoveProductValidation extends TestCase
{
    /**
     * @var array $rules
     */
    private $rules;

    /**
     * @var \Illuminate\Validation\Validator $validator
     */
    private $validator;

    private $data = [
        "g_id" => 1
    ];

    protected function setUp():void {
        parent::setUp();
        $this->validator = app()->get('validator');
        $this->rules = RemoveProductRequestValidator::RULES;
    }

    /**
     * @return array
     */
    public function validationProvider() {
        return [
            'request.success' => [
                'passed' => true,
                'request' => $this->data
            ],
            'g_id.validation.required' => [
                'passed' => false,
                'request' => array_merge($this->data, ['g_id' => ''])
            ],
            'g_id.validation.numeric' => [
                'passed' => false,
                'request' => array_merge($this->data, ['g_id' => 'test'])
            ],
            'g_id.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data, ['g_id' => rand(1,100)])
            ]
        ];
    }

    /**
     * @test
     * @dataProvider validationProvider
     * @param bool $shouldPass
     * @param array $mockedRequestData
     */
    public function validation_results_as_expected($shouldPass, $mockedRequestData)
    {
        $this->assertEquals(
            $shouldPass,
            $this->validate($mockedRequestData)
        );
    }

    /**
     * @param $mockedRequestData
     * @return mixed
     */
    protected function validate($mockedRequestData)
    {
        return $this->validator
            ->make($mockedRequestData, $this->rules)
            ->passes();
    }
}

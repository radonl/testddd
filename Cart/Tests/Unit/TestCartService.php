<?php

namespace Tests\Unit;

use Deka\Cart\Domain\Entity\CartEntity;
use Deka\Cart\Domain\Entity\ProductEntity;
use Deka\Cart\Domain\Interfaces\CartEntityInterface;
use Illuminate\Support\Collection;
use Tests\TestCase;

/**
 * Class TestCartService
 * @package Tests\Unit
 */
class TestCartService extends TestCase
{
    /**
     * @var \Deka\Cart\Domain\Services\CartService
     */
    private $service;

    /**
     * @var \Mockery\MockInterface
     */
    private $cartEntityMock;

    /**
     * @var \Mockery\MockInterface
     */
    private $busMock;

    /**
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp():void {
        parent::setUp();
        $cartEntity = $this->mock(\Deka\Cart\Domain\Entity\CartEntity::class);
        $this->app->bind('Deka\Cart\Domain\Interfaces\CartEntityInterface', function () use ($cartEntity) {
            return $cartEntity;
        });
        $this->cartEntityMock = $cartEntity;
        $bus = $this->mock(\League\Tactician\CommandBus::class);
        $this->app->bind('League\Tactician\CommandBus', function () use ($bus) {
            return $bus;
        });
        $this->busMock = $bus;
        $this->service = $this->app->make('\Deka\Cart\Domain\Services\CartService');

    }

    public function testRemove() {
        $id = random_int(1,100);
        $this->cartEntityMock->shouldReceive('removeProduct')->with($id);
        $this->cartEntityMock->shouldReceive('save');
        $this->service->remove($id);
    }

    public function testGetProductsCount() {
        $count = random_int(1,100);
        $this->cartEntityMock->shouldReceive('getProductsCount')->andReturn($count);
        $this->assertTrue($this->service->getProductsCount() == $count);
    }

    public function testAdd() {
        $id = random_int(1, 100);
        $count = random_int(1, 100);
        $this->busMock->shouldReceive('handle')->andReturn([$id]);
        $this->cartEntityMock->shouldReceive('addProduct')->with([$id], $count);
        $this->cartEntityMock->shouldReceive('save');
        $this->service->add($id, $count);
    }
}

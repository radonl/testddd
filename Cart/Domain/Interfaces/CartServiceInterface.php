<?php

namespace Deka\Cart\Domain\Interfaces;
use Deka\Cart\Domain\Entity\CartEntity;
use Deka\Cart\Domain\Entity\ProductEntity;

/**
 * Interface CartServiceInterface
 * @package Deka\Cart\Domain\Interfaces
 */
interface CartServiceInterface {
    /**
     * @return CartEntity
     */
    public function getCart(): CartEntity;

    /**
     * @param int $id
     * @param int $quantity
     * @return ProductEntity
     */
    public function add(int $id, int $quantity): ProductEntity;

    /**
     * @param int $id
     */
    public function remove(int $id): void;

    /**
     * @return int
     */
    public function getProductsCount(): int;
}

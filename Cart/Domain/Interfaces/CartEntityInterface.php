<?php

namespace Deka\Cart\Domain\Interfaces;

use Deka\Cart\Domain\Entity\ProductEntity;
use Illuminate\Support\Collection;

/**
 * Interface CartEntityInterface
 * @package Deka\Cart\Domain\Interfaces
 */
interface CartEntityInterface {
    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * @return Collection
     */
    public function getProducts(): Collection;

    /**
     * @param array $product
     * @param int $quantity
     * @return ProductEntity
     */
    public function addProduct(array $product, int $quantity): ProductEntity;

    /**
     * @param int $g_id
     */
    public function removeProduct(int $g_id): void;

    /**
     * @return int
     */
    public function getProductsCount(): int;

    /**
     * save
     */
    public function save(): void ;
}

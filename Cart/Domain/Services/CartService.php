<?php

namespace Deka\Cart\Domain\Services;

use Deka\Cart\Domain\Entity\CartEntity;
use Deka\Cart\Domain\Entity\ProductEntity;
use Deka\Cart\Domain\Interfaces\CartEntityInterface;
use Deka\Cart\Domain\Interfaces\CartServiceInterface;
use Deka\Goods\Application\Services\Request\GetProduct;
use League\Tactician\CommandBus;

/**
 * Class CartService
 * @package Deka\Cart\Domain\Services
 */
class CartService implements CartServiceInterface {

    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * @var CartEntity
     */
    private $cart;

    /**
     * CartService constructor.
     * @param CommandBus $bus
     * @param CartEntityInterface $cartEntity
     */
    public function __construct(
        CommandBus $bus,
        CartEntityInterface $cartEntity
    )
    {
        $this->bus = $bus;
        $this->cart = $cartEntity;
    }

    /**
     * @return CartEntity
     */
    public function getCart(): CartEntity {
        return $this->cart;
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function remove(int $id): void
    {
        $this->cart->removeProduct($id);
        $this->cart->save();
    }

    /**
     * @return int
     */
    public function getProductsCount(): int
    {
        return $this->cart->getProductsCount();
    }

    /**
     * @param int $id
     * @param int $quantity
     * @return ProductEntity
     * @throws \Exception
     */
    public function add(int $id, int $quantity): ProductEntity
    {
        $product = $this->bus->handle(new GetProduct($id));
        $response = $this->cart->addProduct($product, $quantity);
        $this->cart->save();
        return $response;
    }
}


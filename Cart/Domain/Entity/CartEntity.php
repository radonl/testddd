<?php

namespace Deka\Cart\Domain\Entity;

use Deka\Cart\Domain\Interfaces\CartEntityInterface;
use Deka\Cart\Events\AddProductEvent;
use Deka\Cart\Events\RemoveProductEvent;
use Deka\Cart\Infrastructure\Interfaces\CartRepositoryInterface;
use Deka\Goods\Application\Services\Request\GetProduct;
use Exception;
use Illuminate\Support\Collection;
use League\Tactician\CommandBus;

/**
 * Class CartEntity
 * @package Deka\Cart\Domain\Entity
 */
class CartEntity implements CartEntityInterface {

    /**
     * @var Collection
     */
    private $products;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CartEntity constructor.
     * @param CartRepositoryInterface $cartRepository
     * @param CommandBus $bus
     */
    public function __construct(
        CartRepositoryInterface $cartRepository,
        CommandBus $bus
    )
    {
        $this->cartRepository = $cartRepository;
        $this->bus = $bus;
        $cart = $this->cartRepository->getCart();
        if($cart && $cart->products) {
            $this->products = collect($cart->products)->map(function ($item, $key) {
                return new ProductEntity(array_merge($this->bus->handle(new GetProduct($key)), [
                    'count' => $item->count
                ]));
            });
        } else {
            $this->products = collect([]);
        }
    }

    /**
     * @return array
     */
    public function toArray(): array {
        return [
            'products' => $this->products->toArray()
        ];
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @param array $product
     * @param int $quantity
     * @return ProductEntity
     */
    public function addProduct(array $product, int $quantity): ProductEntity {
        if($this->products->has($product['goods_id'])) {
            $this->products->get($product['goods_id'])->addCount($quantity);
        } else {
            $this->products->put($product['goods_id'], new ProductEntity(array_merge($product, [
                'count' => $quantity
            ])));
        }
        event(new AddProductEvent($this->products->get($product['goods_id'])));
        return $this->products->get($product['goods_id']);
    }

    /**
     * @param int $g_id
     * @throws Exception
     */
    public function removeProduct(int $g_id): void {
        if($this->products->has($g_id)) {
            event(new RemoveProductEvent($this->products->get($g_id)));
            $this->products->forget($g_id);
        } else {
            throw new Exception('Product not in cart');
        }
    }

    /**
     * @return int
     */
    public function getProductsCount(): int {
        $count = 0;
        $this->products->each(function ($item) use (&$count) {
            $count += $item->getCount();
        });
        return $count;
    }

    /**
     * Save cart
     */
    public function save(): void {
        $this->cartRepository->saveCart($this);
    }
}

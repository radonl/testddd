<?php

namespace Deka\Cart\Domain\Entity;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Class ProductEntity
 * @package Deka\Cart\Domain\Entity
 */
class ProductEntity implements Arrayable{

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $old_price;

    /**
     * @var int
     */
    private $price;

    /**
     * @var int
     */
    private $discount_percent;

    /**
     * @var int
     */
    private $brandId;

    /**
     * @var int|null
     */
    private $categoryId;

    /**
     * @var int|null
     */
    private $count;

    /**
     * ProductEntity constructor.
     * @param $item
     */
    public function __construct($item)
    {
        if(!is_array($item)) {
            $item = (array) $item;
        }
        $this->id = $item['id'];
        $this->name = $item['name'];
        $this->old_price = $item['old_price'];
        $this->price = $item['price'];
        $this->discount_percent = $item['discount_percent'];
        $this->count = $item['count'];
        if(array_key_exists('props', $item) && array_key_exists('values', $item['props'])) {
            if (array_key_exists('brand', $item['props']['values'])) {
                $this->brandId = $item['props']['values']['brand'][0]['id'];
            }
            if (array_key_exists('category', $item['props']['values'])) {
                $this->categoryId = $item['props']['values']['category'][0]['id'];
            }
        } else {
            $this->brandId = $item['brandId'];
            $this->categoryId = $item['categoryId'];
        }
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
          'id' => $this->id,
          'name' => $this->name,
          'old_price' => $this->old_price,
          'price' => $this->price,
          'discount_percent' => $this->discount_percent,
          'count' => $this->count,
          'categoryId' => $this->categoryId,
          'brandId' => $this->brandId
        ];
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    /**
     * @param int $count
     */
    public function addCount(int $count) {
        $this->count += $count;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getOldPrice(): int
    {
        return $this->old_price;
    }

    /**
     * @return null|int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getDiscountPercent(): int
    {
        return $this->discount_percent;
    }

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    /**
     * @return int
     */
    public function getBrandId(): int
    {
        return $this->brandId;
    }
}

<?php

namespace Deka\Cart\Infrastructure\Interfaces;

use Deka\Cart\Domain\Entity\CartEntity;

/**
 * Interface CartRepositoryInterface
 * @package Deka\Cart\Infrastructure\Interfaces
 */
interface CartRepositoryInterface {

    /**
     * @return mixed
     */
    public function getCart();

    /**
     * @param CartEntity $cartEntity
     * @return mixed
     */
    public function saveCart(CartEntity $cartEntity);
}

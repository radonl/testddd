<?php

namespace Deka\Cart\Infrastructure\Repositories;

use Deka\Cart\Domain\Entity\CartEntity;
use Deka\Cart\Domain\Entity\ProductEntity;
use Deka\Cart\Infrastructure\Interfaces\CartRepositoryInterface;
use Illuminate\Cookie\CookieJar;

use Illuminate\Support\Facades\Cookie;


/**
 * Class CartRepository
 * @package Deka\Cart\Infrastructure\Repositories
 */
class CartRepository implements CartRepositoryInterface {

    /**
     * @var CookieJar
     */
    private $cookieJar;

    /**
     * CartRepository constructor.
     * @param CookieJar $cookieJar
     */
    public function __construct(CookieJar $cookieJar)
    {
        $this->cookieJar = $cookieJar;
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return json_decode(Cookie::get('cart'));
    }

    /**
     * @param CartEntity $cartEntity
     */
    public function saveCart(CartEntity $cartEntity)
    {
        Cookie::queue(cookie('cart', json_encode([
            'products' => $cartEntity->getProducts()->map(function (ProductEntity $item) {
                return [
                    'count' => $item->getCount()
                ];
            })
        ]), 525960));
    }
}

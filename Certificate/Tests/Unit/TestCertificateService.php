<?php

namespace Tests\Unit;

use Deka\Certificate\Infrastructure\Interfaces\CertificateReadRepositoryInterface;
use League\Tactician\CommandBus;
use Tests\TestCase;

/**
 * Class TestCartService
 * @package Tests\Unit
 */
class TestCertificateService extends TestCase
{
    /**
     * @var \Deka\Certificate\Domain\Services\CertificateService
     */
    private $service;

    /**
     * @var \Mockery\MockInterface
     */
    private $certificateReadRepositoryMock;

    /**
     * @var \Mockery\MockInterface
     */
    private $busMock;

    /**
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp():void {
        parent::setUp();
        $certificateReadRepository = $this->mock(CertificateReadRepositoryInterface::class);
        $this->app->bind('Deka\Certificate\Infrastructure\Interfaces\CertificateReadRepositoryInterface', function () use ($certificateReadRepository) {
            return $certificateReadRepository;
        });
        $this->certificateReadRepositoryMock = $certificateReadRepository;
        $bus = $this->mock(CommandBus::class);
        $this->app->bind('League\Tactician\CommandBus', function () use ($bus) {
            return $bus;
        });
        $this->busMock = $bus;
        $this->service = $this->app->make('\Deka\Certificate\Domain\Services\CertificateService');

    }

    public function testCheckCertificate() {
        $phone = '+380(11)111-11-11';
        $certificate = 'test';
        $this->certificateReadRepositoryMock
            ->shouldReceive('getCertificate')
            ->with($certificate, $phone)
            ->andReturn([]);
        $this->service->checkCertificate($certificate, $phone);
    }
}

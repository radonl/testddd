<?php

namespace Tests\Unit;

use Deka\Cart\Application\Validation\AddProductRequestValidator;
use Deka\Cart\Application\Validation\RemoveProductRequestValidator;
use Deka\Certificate\Application\Validation\CertificateCheckValidator;
use Tests\TestCase;

/**
 * Class TestsUnitAddProductValidation
 * @package Tests\Unit
 */
class TestCertificateCheckValidation extends TestCase
{
    /**
     * @var array $rules
     */
    private $rules;

    /**
     * @var \Illuminate\Validation\Validator $validator
     */
    private $validator;

    private $data = [
        'phone' => '+380(11)111-11-11',
        'certificate' => 'test'
    ];

    protected function setUp():void {
        parent::setUp();
        $this->validator = app()->get('validator');
        $this->rules = CertificateCheckValidator::RULES;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function validationProvider() {
        return [
            'request.success' => [
                'passed' => true,
                'request' => $this->data
            ],
            'certificate.validation.required' => [
                'passed' => false,
                'request' => array_merge($this->data, ['certificate' => ''])
            ],
            'certificate.validation.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['certificate' => random_int(1,100)])
            ],
            'certificate.validation.max' => [
                'passed' => false,
                'request' => array_merge($this->data, ['certificate' => str_random(512)])
            ],
            'certificate.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data, ['certificate' => str_random(10)])
            ],
            'phone.validation.required' => [
                'passed' => false,
                'data' => array_merge($this->data, ['phone' => ''])
            ],
            'phone.validation.string' => [
                'passed' => false,
                'data' => array_merge($this->data, ['phone' => 12345])
            ],
            'phone.validation.regex' => [
                'passed' => false,
                'data' => array_merge($this->data, ['phone' => '+380'])
            ],
            'phone.validation.success' => [
                'passed' => true,
                'data' => array_merge($this->data,['phone' =>  '+380(11)111-11-11'])
            ],
        ];
    }

    /**
     * @test
     * @dataProvider validationProvider
     * @param bool $shouldPass
     * @param array $mockedRequestData
     */
    public function validation_results_as_expected($shouldPass, $mockedRequestData)
    {
        $this->assertEquals(
            $shouldPass,
            $this->validate($mockedRequestData)
        );
    }

    /**
     * @param $mockedRequestData
     * @return mixed
     */
    protected function validate($mockedRequestData)
    {
        return $this->validator
            ->make($mockedRequestData, $this->rules)
            ->passes();
    }
}

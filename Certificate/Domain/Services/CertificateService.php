<?php

namespace Deka\Certificate\Domain\Services;

use Carbon\Carbon;
use Deka\Certificate\Application\Events\CertificateChecked;
use Deka\Certificate\Domain\Interfaces\CertificateServiceInterface;
use Deka\Certificate\Infrastructure\Interfaces\CertificateReadRepositoryInterface;
use League\Tactician\CommandBus;

/**
 * Class CertificateService
 * @package Deka\Certificate\Domain\Services
 */
class CertificateService implements CertificateServiceInterface {

    /**
     * @var CertificateReadRepositoryInterface
     */
    private $certificateReadRepository;

    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CertificateService constructor.
     * @param CertificateReadRepositoryInterface $certificateReadRepository
     * @param CommandBus $bus
     */
    public function __construct(
        CertificateReadRepositoryInterface $certificateReadRepository,
        CommandBus $bus
    )
    {
        $this->certificateReadRepository = $certificateReadRepository;
        $this->bus = $bus;
    }

    /**
     * @param string $certificate
     * @param string $phone
     * @return array
     */
    public function checkCertificate(string $certificate, string $phone): array
    {
        $certificateRow = $this->certificateReadRepository->getCertificate($certificate, $phone);
        $errors = [];
        $now = Carbon::now();
        if(!$certificateRow) {
            $errors['does-not-exist'] = true;
        } else {
            if($certificateRow['active_to'] < $now) {
                $errors['expired'] = true;
            }
            if($certificateRow['active_from'] > $now) {
                $errors['not-yet-acted'] = true;
            }
            if($certificateRow['phone'] != $phone) {
                $errors['phone-is-incorrect'] = true;
            }
        }
        event(new CertificateChecked($certificate));
        if(!$errors) {
            return [
                'status' => 'success',
                'certificate' => [
                    'name' => $certificateRow['name'],
                    'code' => $certificateRow['code'],
                    'discount_amount' => $certificateRow['summ']
                ],
                'error' => []
            ];
        } else {
            return [
                'status' => 'failed',
                'error' => [
                    'certificate' => $errors
                ]
            ];
        }
    }
}

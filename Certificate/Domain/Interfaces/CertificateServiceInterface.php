<?php

namespace Deka\Certificate\Domain\Interfaces;

/**
 * Interface CertificateServiceInterface
 * @package Deka\Certificate\Domain\Interfaces
 */
interface CertificateServiceInterface {

    /**
     * @param string $certificate
     * @param string $phone
     * @return array
     */
    public function checkCertificate(string $certificate, string $phone): array;
}

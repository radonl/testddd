<?php

namespace Deka\Certificate\Infrastructure\Repositories;

use Deka\Certificate\Infrastructure\Interfaces\CertificateReadRepositoryInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class CertificateReadRepository
 * @package Deka\Certificate\Infrastructure\Repositories
 */
class CertificateReadRepository implements  CertificateReadRepositoryInterface {

    /**
     * @param string $code
     * @param string $phone
     * @return array
     */
    public function getCertificate(string $code, string $phone): array
    {
        return (array)DB::table('checkout_certificate')
            ->where('code', $code)
            ->where('phone', $phone)
            ->first();
    }
}

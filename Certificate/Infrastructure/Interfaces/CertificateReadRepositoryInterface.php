<?php

namespace Deka\Certificate\Infrastructure\Interfaces;


/**
 * Interface CertificateReadRepositoryInterface
 * @package Deka\Certificate\Infrastructure\Interfaces
 */
interface CertificateReadRepositoryInterface
{
    /**
     * @param string $code
     * @param string $phone
     * @return array
     */
    public function getCertificate(string $code, string $phone): array;
}

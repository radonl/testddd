<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->middleware('api')
    ->namespace("Deka\Certificate\UI\Controllers")
    ->group(function () {
        Route::post('/checkout/order/certificate/check/', "CertificateController@checkCertificate");
    });

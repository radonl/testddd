<?php

namespace Deka\Certificate\UI\Controllers;


use Deka\Certificate\Application\Services\Request\CheckCertificate;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use League\Tactician\CommandBus;

/**
 * Class CertificateController
 * @package Deka\Certificate\UI\Controllers
 */
class CertificateController extends Controller
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CommentsController constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus) {
        $this->bus = $bus;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkCertificate(Request $request) {
        $request = new CheckCertificate($request->toArray());
        if ($request->getValidator()->fails()) {
            return response()->json([
              'status' => false,
              'errors' => $request->getValidator()->errors()
            ], 422);
        } else {
            return response()->json($this->bus->handle($request), 200);
        }

    }
}

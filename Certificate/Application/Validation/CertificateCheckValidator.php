<?php

namespace Deka\Certificate\Application\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

/**
 * Class CertificateCheckValidator
 * @package Deka\Certificate\Application\Validation
 */
class CertificateCheckValidator extends FormRequest
{
    const RULES = [
        'certificate' => 'required|string|max:255',
        'phone' => 'required|string|regex:/^\+380\(\d{2}\)\d{3}\-\d{2}\-\d{2}$/',
    ];

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($request){
        return Validator::make($request, CertificateCheckValidator::RULES);
    }
}

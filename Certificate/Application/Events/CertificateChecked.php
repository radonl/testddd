<?php

namespace Deka\Certificate\Application\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class CertificateChecked
 * @package Deka\Certificate\Application\Events
 */
class CertificateChecked
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $certificate;

    /**
     * Create a new event instance.
     *
     * @param string $certificate
     */
    public function __construct(string $certificate)
    {
        $this->certificate = $certificate;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        //ToDo Must be updated as required
        return new PrivateChannel('channel-name');
    }
}

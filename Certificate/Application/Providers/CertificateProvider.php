<?php

namespace Deka\Certificate\Application\Providers;

use App\Domain\Interfaces\CityCertificateServiceInterface;
use Deka\Certificate\Domain\Interfaces\CertificateServiceInterface;
use Deka\Certificate\Domain\Services\CertificateService;
use Deka\Certificate\Infrastructure\Interfaces\CertificateReadRepositoryInterface;
use Deka\Certificate\Infrastructure\Repositories\CertificateReadRepository;
use Deka\Common\Application\AbstractServiceProvider;

/**
 * Class CertificateProvider
 * @package Deka\Certificate\Application\Providers
 */
class CertificateProvider extends AbstractServiceProvider
{
    public function register()
    {
        $this->app->singleton(CertificateReadRepositoryInterface::class, CertificateReadRepository::class);
        $this->app->singleton(CityCertificateServiceInterface::class, CertificateService::class);
        $this->app->singleton(CertificateServiceInterface::class, CertificateService::class);
    }

    protected $migrationsPath = __DIR__ . '/../../Infrastructure/Migrations';

    protected $routesPath = __DIR__ . '/../../UI/Routes/api.php';
}

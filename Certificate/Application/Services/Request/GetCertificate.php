<?php
namespace Deka\Certificate\Application\Services\Request;

/**
 * Class GetCertificate
 * @package Deka\Certificate\Application\Services\Request
 */
class GetCertificate {

    /**
     * @var string
     */
    private $certificate;

    /**
     * @var mixed|null
     */
    private $phone;

    /**
     * GetCertificate constructor.
     * @param array $request
     */
    public function __construct(array $request)
    {
       $this->certificate = $request['certificate'] ?? null;
       $this->phone = $request['phone'] ?? null;
    }

    /**
     * @return string
     */
    public function getCertificate(): string
    {
        return $this->certificate;
    }

    /**
     * @return mixed|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }
}

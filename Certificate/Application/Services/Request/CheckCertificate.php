<?php
namespace Deka\Certificate\Application\Services\Request;

use Deka\Certificate\Application\Validation\CertificateCheckValidator;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class CheckCertificate
 * @package Deka\Certificate\Application\Services\Request
 */
class CheckCertificate {

    /**
     * @var string|null
     */
    private $certificate;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * CheckCertificate constructor.
     * @param array $request
     */
    public function __construct(array $request)
    {
       $this->certificate = $request['certificate'] ?? null;
       $this->phone = $request['phone'] ?? null;
       $this->validator = CertificateCheckValidator::make($this->toArray());
    }

    /**
     * @return array
     */
    private function toArray() {
        return [
            'certificate' => $this->certificate,
            'phone' => $this->phone
        ];
    }

    /**
     * @return string|null
     */
    public function getCertificate(): ?string
    {
        return $this->certificate;
    }

    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }
}

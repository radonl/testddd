<?php

namespace Deka\Certificate\Application\Services;

use Deka\Certificate\Application\Services\Request\CheckCertificate;
use Deka\Certificate\Domain\Interfaces\CertificateServiceInterface;

/**
 * Class CheckCertificateHandler
 * @package Deka\Certificate\Application\Services
 */
class CheckCertificateHandler
{

    /**
     * @var CertificateServiceInterface
     */
    private $certificateService;

    /**
     * CheckCertificateHandler constructor.
     * @param CertificateServiceInterface $certificateService
     */
    public function __construct(CertificateServiceInterface $certificateService)
    {
        $this->certificateService = $certificateService;
    }

    /**
     * @param CheckCertificate $request
     * @return array
     */
    public function handle(CheckCertificate $request)
    {
        return $this->certificateService->checkCertificate($request->getCertificate(), $request->getPhone());
    }
}

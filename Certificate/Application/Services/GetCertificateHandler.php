<?php

namespace Deka\Certificate\Application\Services;

use Deka\Certificate\Application\Services\Request\GetCertificate;
use Deka\Certificate\Infrastructure\Interfaces\CertificateReadRepositoryInterface;

/**
 * Class GetCertificateHandler
 * @package Deka\Certificate\Application\Services
 */
class GetCertificateHandler
{

    /**
     * @var CertificateReadRepositoryInterface
     */
    private $certificateReadRepository;

    /**
     * GetCertificateHandler constructor.
     * @param CertificateReadRepositoryInterface $certificateReadRepository
     */
    public function __construct(CertificateReadRepositoryInterface $certificateReadRepository)
    {
        $this->certificateReadRepository = $certificateReadRepository;
    }

    /**
     * @param GetCertificate $request
     * @return array
     */
    public function handle(GetCertificate $request)
    {
        return $this->certificateReadRepository->getCertificate(
            $request->getCertificate(),
            $request->getPhone()
        );
    }
}

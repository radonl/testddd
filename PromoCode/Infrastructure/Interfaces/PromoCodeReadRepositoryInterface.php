<?php

namespace Deka\PromoCode\Infrastructure\Interfaces;

/**
 * Interface PromoCodeReadRepositoryInterface
 * @package Deka\PromoCode\Infrastructure\Interfaces
 */
interface PromoCodeReadRepositoryInterface
{
    /**
     * @param $promoCode
     * @return null|object
     */
    public function getPromoCode($promoCode): ?object;
}

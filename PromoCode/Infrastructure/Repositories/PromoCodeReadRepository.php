<?php

namespace Deka\PromoCode\Infrastructure\Repositories;

use Deka\PromoCode\Infrastructure\Interfaces\PromoCodeReadRepositoryInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class PromoCodeReadRepository
 * @package Deka\PromoCode\Infrastructure\Repositories
 */
class PromoCodeReadRepository implements  PromoCodeReadRepositoryInterface {

    /**
     * @param $promoCode
     * @return null|object
     */
    public function getPromoCode($promoCode): ?object
    {
        return DB::table('checkout_promo_code')->where('code', $promoCode)->first();
    }
}

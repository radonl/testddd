<?php

namespace Deka\PromoCode\Domain\Interfaces;

/**
 * Interface PromoCodeServiceInterface
 * @package Deka\PromoCode\Domain\Interfaces
 */
interface PromoCodeServiceInterface {
    /**
     * @param string $promoCode
     * @return array
     */
    public function checkPromoCode(string $promoCode): array ;
}

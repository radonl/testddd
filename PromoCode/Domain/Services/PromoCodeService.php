<?php

namespace Deka\PromoCode\Domain\Services;

use Carbon\Carbon;
use Deka\Cart\Services\Request\GetAllProducts;
use Deka\PromoCode\Application\Events\PromoCodeChecked;
use Deka\PromoCode\Domain\Interfaces\PromoCodeServiceInterface;
use Deka\PromoCode\Infrastructure\Interfaces\PromoCodeReadRepositoryInterface;
use League\Tactician\CommandBus;

/**
 * Class PromoCodeService
 * @package Deka\PromoCode\Domain\Services
 */
class PromoCodeService implements PromoCodeServiceInterface {

    /**
     * @var PromoCodeReadRepositoryInterface
     */
    private $promoCodeReadRepository;

    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * PromoCodeService constructor.
     * @param PromoCodeReadRepositoryInterface $promoCodeReadRepository
     * @param CommandBus $bus
     */
    public function __construct(
        PromoCodeReadRepositoryInterface $promoCodeReadRepository,
        CommandBus $bus
    )
    {
        $this->promoCodeReadRepository = $promoCodeReadRepository;
        $this->bus = $bus;
    }

    /**
     * @param string $promoCode
     * @return array
     */
    public function checkPromoCode(string $promoCode): array
    {
        $promoCodeRow = $this->promoCodeReadRepository->getPromoCode($promoCode);
        $promoCodeIsApplicableToCart = false;
        $discountedProducts = [];
        $notDiscountedProducts = [];
        $errors = [];
        $now = Carbon::now();
        if(!$promoCodeRow) {
            $errors['does-not-exist'] = true;
        } else {
            if($promoCodeRow->active_to < $now) {
                $errors['expired'] = true;
            }
            if($promoCodeRow->active_from > $now) {
                $errors['not-yet-acted'] = true;
            }
            foreach($this->bus->handle(new GetAllProducts()) as $key => $item) {
                if(
                    $promoCodeRow && $key == $promoCodeRow->product_id ||
                    $promoCodeRow->brand_id == $item->getBrandId() ||
                    $promoCodeRow->category_id == $item->getCategoryId()
                ) {
                    $discountedProducts[$key] = $item;
                    $promoCodeIsApplicableToCart = true;
                } else {
                    $notDiscountedProducts[$key] = $item;
                }
            }
            if(!$promoCodeIsApplicableToCart) {
                $errors['is-not-applicable-to-cart'] = true;
            }
        }

        event(new PromoCodeChecked($promoCode));
        if(!$errors) {
            return [
                'status' => 'success',
                'promo-code' => [
                    'name' => $promoCodeRow->name,
                    'code' => $promoCodeRow->code
                ],
                'summary' => $this->getSummary($discountedProducts, $notDiscountedProducts, $promoCodeRow),
                'error' => []
            ];
        } else {
            return [
                'status' => 'failed',
                'error' => [
                    'promoCode' => $errors
                ]
            ];
        }
    }

    /**
     * @param array $discountedProducts
     * @param array $notDiscountedProducts
     * @param object $promoCodeRow
     * @return array
     */
    private function getSummary(array $discountedProducts, array $notDiscountedProducts, object $promoCodeRow): array {
        $price = 0;
        $old_price = 0;
        $count = 0;
        $products = [];
        foreach ($discountedProducts as $product) {
            $product = $product->toArray();
            $product['price'] = $product['price'] - ($product['price'] * $promoCodeRow->percent / 100);
            $product['discount_percent'] = round(100 - ($product['price'] / $product['old_price'] * 100), 2);
            array_push($products, $product);
            $price += $product['price'];
            $old_price += $product['old_price'];
            $count += $product['count'];
        }
        foreach ($notDiscountedProducts as $product) {
            $product = $product->toArray();
            array_push($products, $product);
            $price += $product['price'];
            $old_price += $product['old_price'];
            $count += $product['count'];
        }
        return [
            'items' => $products,
            'info' => [
                'discount_percent' => round($price > 0 ? (100 - ($price / $old_price * 100)) : 0 , 2),
                'count' => $count,
                'total' => $price
            ]
        ];
    }
}

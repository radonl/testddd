<?php

namespace Tests\Unit;

use Deka\Cart\Application\Validation\AddProductRequestValidator;
use Deka\Cart\Application\Validation\RemoveProductRequestValidator;
use Deka\Certificate\Application\Validation\CertificateCheckValidator;
use Deka\Delivery\Application\Validation\GetBranchesValidator;
use Deka\Order\Application\Validation\CreateNewOrderRequestValidator;
use Deka\Order\Application\Validation\OneClickBuyRequestValidator;
use Deka\PromoCode\Application\Validation\PromoCodeCheckValidator;
use Faker\Factory;
use Tests\TestCase;

/**
 * Class TestsUnitAddProductValidation
 * @package Tests\Unit
 */
class TestPromoCodeValidation extends TestCase
{
    /**
     * @var array $rules
     */
    private $rules;

    /**
     * @var \Illuminate\Validation\Validator $validator
     */
    private $validator;

    private $data = [
        'promoCode' => 'TEST-TEST'
    ];

    protected function setUp():void {
        parent::setUp();
        $this->validator = app()->get('validator');
        $this->rules = PromoCodeCheckValidator::RULES;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function validationProvider() {
        return [
            'request.success' => [
                'passed' => true,
                'request' => $this->data
            ],
            'promoCode.validation.required' => [
                'passed' => false,
                'request' => array_merge($this->data, ['promoCode' => ''])
            ],
            'promoCode.validation.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['promoCode' => 12345])
            ],
            'promoCode.validation.regex' => [
                'passed' => false,
                'request' => array_merge($this->data, ['promoCode' => 'TEST'])
            ],
            'promoCode.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data,['promoCode' =>  'TEST-TEST'])
            ]
        ];
    }

    /**
     * @test
     * @dataProvider validationProvider
     * @param bool $shouldPass
     * @param array $mockedRequestData
     */
    public function validation_results_as_expected($shouldPass, $mockedRequestData)
    {
        $this->assertEquals(
            $shouldPass,
            $this->validate($mockedRequestData)
        );
    }

    /**
     * @param $mockedRequestData
     * @return mixed
     */
    protected function validate($mockedRequestData)
    {
        return $this->validator
            ->make($mockedRequestData, $this->rules)
            ->passes();
    }
}

<?php

namespace Tests\Unit;

use Deka\PromoCode\Infrastructure\Repositories\PromoCodeReadRepository;
use League\Tactician\CommandBus;
use Tests\TestCase;

/**
 * Class TestCartService
 * @package Tests\Unit
 */
class TestPromoCodeService extends TestCase
{
    /**
     * @var \Deka\PromoCode\Domain\Services\PromoCodeService
     */
    private $service;

    /**
     * @var \Mockery\MockInterface
     */
    private $promoCodeReadRepositoryMock;

    /**
     * @var \Mockery\MockInterface
     */
    private $busMock;

    /**
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp():void {
        parent::setUp();
        $promoCodeReadRepository = $this->mock(PromoCodeReadRepository::class);
        $this->app->bind('Deka\PromoCode\Infrastructure\Repositories\PromoCodeReadRepository', function () use ($promoCodeReadRepository) {
            return $promoCodeReadRepository;
        });
        $this->promoCodeReadRepositoryMock = $promoCodeReadRepository;
        $busMock = $this->mock(CommandBus::class);
        $this->app->bind('League\Tactician\CommandBus', function () use ($busMock) {
            return $busMock;
        });
        $this->busMock = $busMock;
        $this->service = $this->app->make('\Deka\PromoCode\Domain\Services\PromoCodeService');
    }

    public function testCheckPromoCode() {
        $promoCode = 'TEST-TEST';
        $this->promoCodeReadRepositoryMock
            ->shouldReceive('getPromoCode')
            ->with($promoCode)
            ->andReturn(null);
        $this->assertTrue($this->service->checkPromoCode($promoCode)['status'] == 'failed');
    }
}

<?php

namespace Deka\PromoCode\Application\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class PromoCodeChecked
 * @package Deka\PromoCode\Application\Events
 */
class PromoCodeChecked
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $promoCode;

    /**
     * Create a new event instance.
     *
     * @param string $promoCode
     */
    public function __construct(string $promoCode)
    {
        $this->promoCode = $promoCode;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        //ToDo Must be updated as required
        return new PrivateChannel('channel-name');
    }
}

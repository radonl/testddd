<?php

namespace Deka\PromoCode\Application\Providers;

use Deka\Common\Application\AbstractServiceProvider;
use Deka\PromoCode\Domain\Interfaces\PromoCodeServiceInterface;
use Deka\PromoCode\Domain\Services\PromoCodeService;
use Deka\PromoCode\Infrastructure\Interfaces\PromoCodeReadRepositoryInterface;
use Deka\PromoCode\Infrastructure\Repositories\PromoCodeReadRepository;

/**
 * Class PromoCodeProvider
 * @package Deka\PromoCode\Application\Providers
 */
class PromoCodeProvider extends AbstractServiceProvider
{
    public function register()
    {
        $this->app->singleton(PromoCodeReadRepositoryInterface::class, PromoCodeReadRepository::class);
        $this->app->singleton(PromoCodeServiceInterface::class, PromoCodeService::class);
    }

    protected $migrationsPath = __DIR__ . '/../../Infrastructure/Migrations';

    protected $routesPath = __DIR__ . '/../../UI/Routes/api.php';
}

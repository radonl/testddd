<?php

namespace Deka\PromoCode\Application\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

/**
 * Class PromoCodeCheckValidator
 * @package Deka\PromoCode\Application\Validation
 */
class PromoCodeCheckValidator extends FormRequest
{
    const RULES = [
        'promoCode' => 'required|string|max:255|regex:/^[A-Z0-9]{4}-[A-Z0-9]{4}$/'
    ];

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($request){
        return Validator::make($request, PromoCodeCheckValidator::RULES);
    }
}

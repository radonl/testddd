<?php

namespace Deka\PromoCode\Application\Services;

use Deka\PromoCode\Application\Services\Request\CheckPromoCode;
use Deka\PromoCode\Domain\Interfaces\PromoCodeServiceInterface;

/**
 * Class CheckPromoCodeHandler
 * @package Deka\PromoCode\Application\Services
 */
class CheckPromoCodeHandler
{

    /**
     * @var PromoCodeServiceInterface
     */
    private $promoCodeService;

    /**
     * CheckPromoCodeHandler constructor.
     * @param PromoCodeServiceInterface $promoCodeService
     */
    public function __construct(PromoCodeServiceInterface $promoCodeService)
    {
        $this->promoCodeService = $promoCodeService;
    }

    /**
     * @param CheckPromoCode $request
     * @return array
     */
    public function handle(CheckPromoCode $request)
    {
        return $this->promoCodeService->checkPromoCode($request->getPromoCode());
    }
}

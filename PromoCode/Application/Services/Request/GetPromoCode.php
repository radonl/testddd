<?php
namespace Deka\PromoCode\Application\Services\Request;

use Deka\PromoCode\Application\Validation\PromoCodeCheckValidator;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class GetPromoCode
 * @package Deka\PromoCode\Application\Services\Request
 */
class GetPromoCode {

    /**
     * @var string
     */
    private $promoCode;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * CheckPromoCode constructor.
     * @param array $request
     */
    public function __construct(array $request)
    {
        $this->promoCode = $request['promoCode'] ?? null;
        $this->validator = PromoCodeCheckValidator::make($this->toArray());
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            'promoCode' => $this->promoCode,
        ];
    }

    /**
     * @return string
     */
    public function getPromoCode(): string
    {
        return $this->promoCode;
    }

    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }
}

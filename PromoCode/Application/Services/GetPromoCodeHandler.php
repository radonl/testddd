<?php

namespace Deka\PromoCode\Application\Services;

use Deka\PromoCode\Application\Services\Request\GetPromoCode;
use Deka\PromoCode\Infrastructure\Interfaces\PromoCodeReadRepositoryInterface;

/**
 * Class GetPromoCodeHandler
 * @package Deka\PromoCode\Application\Services
 */
class GetPromoCodeHandler
{

    /**
     * @var PromoCodeReadRepositoryInterface
     */
    private $promoCodeReadRepository;

    /**
     * GetPromoCodeHandler constructor.
     * @param PromoCodeReadRepositoryInterface $promoCodeReadRepository
     */
    public function __construct(PromoCodeReadRepositoryInterface $promoCodeReadRepository)
    {
        $this->promoCodeReadRepository = $promoCodeReadRepository;
    }

    /**
     * @param GetPromoCode $request
     * @return object
     */
    public function handle(GetPromoCode $request)
    {
        return $this->promoCodeReadRepository->getPromoCode($request->getPromoCode());
    }
}

<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->middleware('api')
    ->namespace("Deka\PromoCode\UI\Controllers")
    ->group(function () {
        Route::post('/checkout/order/promo-code/check/', "PromoCodeController@checkPromoCode");
    });

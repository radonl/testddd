<?php

namespace Deka\PromoCode\UI\Controllers;

use Deka\PromoCode\Application\Services\Request\CheckPromoCode;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use League\Tactician\CommandBus;

/**
 * Class PromoCodeController
 * @package Deka\PromoCode\UI\Controllers
 */
class PromoCodeController extends Controller
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CommentsController constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus) {
        $this->bus = $bus;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkPromoCode(Request $request) {
        $request = new CheckPromoCode($request->toArray());
        if ($request->getValidator()->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $request->getValidator()->errors()
            ],  422);
        } else {
            return response()->json($this->bus->handle($request), 200);
        }
    }
}

<?php

namespace Deka\Order\Domain\Interfaces;

/**
 * Interface OrderServiceInterface
 * @package Deka\Order\Domain\Interfaces
 */
interface OrderServiceInterface {

    /**
     * @param array $requestData
     * @return int
     */
    public function createOrder(array $requestData):int;

    /**
     * @param array $requestData
     * @return int
     */
    public function createOneClickOrder(array $requestData): int;
}

<?php

namespace Deka\Order\Domain\Interfaces;

use Deka\Order\Domain\Entity\CartEntity;
use Illuminate\Support\Collection;

/**
 * Interface OrderEntityInterface
 * @package Deka\Order\Domain\Interfaces
 */
interface OrderEntityInterface {

    /**
     * @param Collection $products
     * @param array $requestData
     */
    public function setData(Collection $products, array $requestData): void;

    /**
     * @return int
     */
    public function save():int;
}

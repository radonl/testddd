<?php

namespace Deka\Order\Domain\Entity;

use Deka\Goods\Application\Services\Request\GetProduct;
use Exception;
use Illuminate\Contracts\Support\Arrayable;
use League\Tactician\CommandBus;

/**
 * Class ProductEntity
 * @package Deka\Order\Domain\Entity
 */
class ProductEntity implements Arrayable {

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $old_price;

    /**
     * @var int
     */
    private $price;

    /**
     * @var int
     */
    private $discount_percent;

    /**
     * @var int
     */
    private $count;

    /**
     * @var int|null
     */
    private $categoryId;

    /**
     * @var int|null
     */
    private $brandId;

    /**
     * ProductEntity constructor.
     * @param $item
     */
    public function __construct($item)
    {
        $arrayItem = is_array($item) ? $item : $item->toArray();
        $this->id = $arrayItem['id'];
        $this->name = $arrayItem['name'];
        $this->old_price = $arrayItem['old_price'];
        $this->price = $arrayItem['price'];
        $this->discount_percent = $arrayItem['discount_percent'];
        $this->count = $arrayItem['count'];
        if(array_key_exists('props', $item) && array_key_exists('values', $item['props'])) {
            if (array_key_exists('brand', $item['props']['values'])) {
                $this->brandId = $item['props']['values']['brand'][0]['id'];
            }
            if (array_key_exists('category', $item['props']['values'])) {
                $this->categoryId = $item['props']['values']['category'][0]['id'];
            }
        } else {
            $this->brandId = $arrayItem['brandId'];
            $this->categoryId = $arrayItem['categoryId'];
        }
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
          'id' => $this->id,
          'name' => $this->name,
          'old_price' => $this->old_price,
          'price' => $this->price,
          'discount_percent' => $this->discount_percent,
          'count' => $this->count,
          'categoryId' => $this->categoryId,
          'brandId' => $this->brandId
        ];
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getOldPrice(): int
    {
        return $this->old_price;
    }

    /**
     * @return null|int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getDiscountPercent(): int
    {
        return $this->discount_percent;
    }
}

<?php

namespace Deka\Order\Domain\Entity;

use Deka\Delivery\Application\Services\Request\SelectDelivery;
use Deka\PriceCalculator\Application\Services\Request\PriceCalculator;
use Deka\Certificate\Application\Services\Request\GetCertificate;
use Deka\Order\Domain\Interfaces\OrderEntityInterface;
use Deka\Order\Infrastructure\Interfaces\GoodsReadRepositoryInterface;
use Deka\Order\Infrastructure\Interfaces\GoodsWriteRepositoryInterface;
use Deka\Order\Infrastructure\Interfaces\OrderWriteRepositoryInterface;
use Deka\PromoCode\Application\Services\Request\GetPromoCode;
use Deka\User\Services\Request\GetUserDiscount;
use Exception;
use Illuminate\Support\Collection;
use League\Tactician\CommandBus;

/**
 * Class OrderEntity
 * @package Deka\Order\Domain\Entity
 */
class OrderEntity implements OrderEntityInterface {

    /**
     * @var null|int
     */
    private $id;

    /**
     * @var PersonEntity
     */
    private $person;

    /**
     * @var null|int
     */
    private $promoCodeId;

    /**
     * @var null|int
     */
    private $certificateId;

    /**
     * @var null|array
     */
    private $promoCode;

    /**
     * @var null|array
     */
    private $certificate;

    /**
     * @var int|null
     */
    private $deliveryServiceId;

    /**
     * @var int|null
     */
    private $priceOfDelivery;

    /**
     * @var int|null
     */
    private $paymentMethodId;

    /**
     * @var Collection
     */
    private $products;

    /**
     * @var OrderWriteRepositoryInterface
     */
    private $orderWriteRepository;

    /**
     * @var GoodsWriteRepositoryInterface
     */
    private $goodsWriteRepository;

    /**
     * @var GoodsReadRepositoryInterface
     */
    private $goodsReadRepository;

    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * OrderEntity constructor.
     * @param OrderWriteRepositoryInterface $orderWriteRepository
     * @param GoodsWriteRepositoryInterface $goodsWriteRepository
     * @param GoodsReadRepositoryInterface $goodsReadRepository
     * @param CommandBus $bus
     */
    public function __construct(
        OrderWriteRepositoryInterface $orderWriteRepository,
        GoodsWriteRepositoryInterface $goodsWriteRepository,
        GoodsReadRepositoryInterface $goodsReadRepository,
        CommandBus $bus
    ) {
        $this->orderWriteRepository = $orderWriteRepository;
        $this->goodsReadRepository = $goodsReadRepository;
        $this->goodsWriteRepository = $goodsWriteRepository;
        $this->bus = $bus;
    }

    /**
     * @param Collection $products
     * @param array $requestData
     * @throws Exception
     */
    public function setData(Collection $products, array $requestData): void {
        $this->products = $products;
        $this->person = new PersonEntity($requestData);
        $this->deliveryServiceId = $requestData['deliveryServiceId'] ?? null;
        if($this->deliveryServiceId) {
            $res = $this->bus->handle(new SelectDelivery($this->deliveryServiceId));
            if(array_key_exists('delivery', $res) && array_key_exists('price', $res['delivery'])) {
                $this->priceOfDelivery = $res['delivery']['price'];
            }
        }
        $this->paymentMethodId = $requestData['paymentMethodId'] ?? null;
        if(array_key_exists('promoCode', $requestData) && $requestData['promoCode']) {
            $promoCode = $this->bus->handle(new GetPromoCode([
                'promoCode' => $requestData['promoCode']
            ]));
            if($promoCode) {
                $this->promoCodeId = $promoCode->id;
                $this->promoCode = $promoCode;
            } else {
                throw new Exception('Incorrect promo code');
            }
        }
        if(array_key_exists('certificate', $requestData) && $requestData['certificate']) {
            $certificate = $this->bus->handle(new GetCertificate([
                'certificate' => $requestData['certificate'],
                'phone' => $requestData['telefon']
            ]));
            if($certificate) {
                $this->certificateId = $certificate['id'];
                $this->certificate = $certificate;
            } else {
                throw new Exception('Incorrect certificate');
            }
        }
    }

    /**
     * @return int
     * @throws Exception
     */
    public function save(): int {
        $this->products = collect($this->bus->handle(
                new PriceCalculator(
                    $this->products->toArray(),
                    $this->bus->handle(new GetUserDiscount([
                        'phone' => $this->person->getTelefon()
                    ])),
                    (array)$this->promoCode
                )
        )['items'])->mapInto(ProductEntity::class);
        try {
            $this->id = $this->orderWriteRepository->createOrder($this);
        } catch (Exception $exception) {
            throw new Exception('internal-error');
        }

        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        return [
            'id' => $this->id,
            'person' => $this->person->toArray(),
            'promoCodeId' => $this->promoCodeId,
            'certificateId' => $this->certificateId,
            'paymentMethodId' => $this->paymentMethodId
        ];
    }

    /**
     * @return PersonEntity
     */
    public function getPerson(): PersonEntity
    {
        return $this->person;
    }

    /**
     * @return int|null
     */
    public function getPromoCodeId(): ?int
    {
        return $this->promoCodeId;
    }

    /**
     * @return int|null
     */
    public function getCertificateId(): ?int
    {
        return $this->certificateId;
    }

    /**
     * @return int|null
     */
    public function getPaymentMethodId(): ?int
    {
        return $this->paymentMethodId;
    }

    /**
     * @return int|null
     */
    public function getDeliveryServiceId(): ?int
    {
        return $this->deliveryServiceId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @return int|null
     */
    public function getPriceOfDelivery(): ?int
    {
        return $this->priceOfDelivery;
    }
}

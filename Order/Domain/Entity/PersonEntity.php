<?php

namespace Deka\Order\Domain\Entity;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Class PersonEntity
 * @package Deka\Order\Domain\Entity
 */
class PersonEntity {

    /**
     * @var null|int
     */
    private $id;

    /**
     * @var string|null
     */
    private $lastname;

    /**
     * @var string|null
     */
    private $firstname;

    /**
     * @var string|null
     */
    private $middlename;

    /**
     * @var string|null
     */
    private $telefon;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var int|null
     */
    private $sluzhba;

    /**
     * @var string|null
     */
    private $adress;

    /**
     * @var int|null
     */
    private $user_id;

    /**
     * PersonEntity constructor.
     * @param array $person
     */
    public function __construct(array $person) {
        $this->lastname = $person['lastname'] ?? null;
        $this->firstname = $person['firstname'] ?? null;
        $this->middlename = $person['middlename'] ?? null;
        $this->telefon = $person['telefon'];
        $this->email = $person['email'] ?? null;
        $this->sluzhba = $person['sluzhba'] ?? null;
        $this->adress = $person['adress'] ?? null;
        $this->user_id = $person['user_id'] ?? null;
    }


    /**
     * @param array $person
     * @return PersonEntity
     */
    public static function create(array $person){
        return new self($person);
    }

    /**
     * @return string
     */
    public function getTelefon(): ?string
    {
        return $this->telefon;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @return string
     */
    public function getMiddlename(): ?string
    {
        return $this->middlename;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return int|null
     */
    public function getSluzhba(): ?int
    {
        return $this->sluzhba;
    }

    /**
     * @return string
     */
    public function getAdress(): ?string
    {
        return $this->adress;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->user_id;
    }
}

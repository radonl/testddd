<?php

namespace Deka\Order\Domain\Services;

use Deka\Cart\Services\Request\GetAllProducts;
use Deka\Goods\Application\Services\Request\GetProduct;
use Deka\Order\Domain\Entity\ProductEntity;
use Deka\Order\Domain\Interfaces\OrderEntityInterface;
use Deka\Order\Domain\Interfaces\OrderServiceInterface;
use League\Tactician\CommandBus;

/**
 * Class OrderService
 * @package Deka\Order\Domain\Services
 */
class OrderService implements OrderServiceInterface {

    /**
     * @var OrderEntityInterface
     */
    private $orderEntity;

    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * OrderService constructor.
     * @param OrderEntityInterface $orderEntity
     * @param CommandBus $bus
     */
    public function __construct(OrderEntityInterface $orderEntity,  CommandBus $bus) {
        $this->orderEntity = $orderEntity;
        $this->bus = $bus;
    }

    /**
     * @param array $requestData
     * @return int
     * @throws \Exception
     */
    public function createOrder(array $requestData):int {
        $this->orderEntity->setData(
            $this->bus->handle(new GetAllProducts())->mapInto(ProductEntity::class),
            $requestData
        );
        return $this->orderEntity->save();
    }

    /**
     * @param array $requestData
     * @return int
     * @throws \Exception
     */
    public function createOneClickOrder(array $requestData): int
    {
        $this->orderEntity->setData(
            collect([array_merge($this->bus->handle(new GetProduct($requestData['g_id'])), [
                'count' => 1
            ])])->mapInto(ProductEntity::class),
            $requestData
        );
        return $this->orderEntity->save();
    }
}

<?php

namespace Deka\Order\Application\Providers;

use Deka\Common\Application\AbstractServiceProvider;
use Deka\Order\Domain\Entity\OrderEntity;
use Deka\Order\Domain\Interfaces\OrderEntityInterface;
use Deka\Order\Domain\Interfaces\OrderServiceInterface;
use Deka\Order\Domain\Services\OrderService;
use Deka\Order\Infrastructure\Interfaces\DeliveryWriteRepositoryInterface;
use Deka\Order\Infrastructure\Interfaces\GoodsReadRepositoryInterface;
use Deka\Order\Infrastructure\Interfaces\GoodsWriteRepositoryInterface;
use Deka\Order\Infrastructure\Interfaces\OrderWriteRepositoryInterface;
use Deka\Order\Infrastructure\Interfaces\PersonWriteRepositoryInterface;
use Deka\Order\Infrastructure\Repositories\DeliveryWriteRepository;
use Deka\Order\Infrastructure\Repositories\GoodsReadRepository;
use Deka\Order\Infrastructure\Repositories\GoodsWriteRepository;
use Deka\Order\Infrastructure\Repositories\OrderWriteRepository;
use Deka\Order\Infrastructure\Repositories\PersonWriteRepository;;

class OrderProvider extends AbstractServiceProvider
{
    public function boot()
    {
        \Validator::extend('certificate', 'Deka\Order\Application\Validation\Rules\CertificateRule@validate');
        \Validator::extend('promoCode', 'Deka\Order\Application\Validation\Rules\PromoCodeRule@validate');
        \Validator::extend('delivery', 'Deka\Order\Application\Validation\Rules\DeliveryRule@validate');
        \Validator::extend('paymentMethod', 'Deka\Order\Application\Validation\Rules\PaymentMethodRule@validate');
    }

    public function register()
    {
        $this->app->singleton(GoodsReadRepositoryInterface::class, GoodsReadRepository::class);
        $this->app->singleton(GoodsWriteRepositoryInterface::class, GoodsWriteRepository::class);
        $this->app->singleton(OrderWriteRepositoryInterface::class, OrderWriteRepository::class);
        $this->app->singleton(PersonWriteRepositoryInterface::class, PersonWriteRepository::class);
        $this->app->singleton(OrderServiceInterface::class, OrderService::class);
        $this->app->singleton(OrderEntityInterface::class, OrderEntity::class);
        $this->app->singleton(DeliveryWriteRepositoryInterface::class, DeliveryWriteRepository::class);
    }

    protected $migrationsPath = __DIR__ . '/../../Infrastructure/Migrations';

    protected $routesPath = __DIR__ . '/../../UI/Routes/api.php';
}

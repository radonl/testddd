<?php

namespace Deka\Order\Application\Services;

use Deka\Order\Application\Services\Request\OneClickBuy;
use Deka\Order\Domain\Interfaces\OrderServiceInterface;
use Exception;

/**
 * Class OneClickBuyHandler
 * @package Deka\Order\Application\Services
 */
class OneClickBuyHandler
{

    /**
     * @var OrderServiceInterface
     */
    private $orderService;

    /**
     * OneClickBuyHandler constructor.
     * @param OrderServiceInterface $orderService
     */
    public function __construct(OrderServiceInterface $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @param OneClickBuy $request
     * @return array
     */
    public function handle(OneClickBuy $request)
    {
        try {
            return [
                'status' => 'success',
                'order' => [
                    'id' => $this->orderService->createOneClickOrder($request->toArray())
                ],
                'errors' => []
            ];
        } catch (Exception $exception) {
            return [
                'status' => 'failed',
                'error' => $exception->getMessage()
            ];
        }

    }
}

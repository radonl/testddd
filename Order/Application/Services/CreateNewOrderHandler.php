<?php

namespace Deka\Order\Application\Services;

use Deka\Order\Application\Services\Request\CreateNewOrder;
use Deka\Order\Domain\Interfaces\OrderServiceInterface;
use Exception;
use League\Tactician\CommandBus;

/**
 * Class CreateNewOrderHandler
 * @package Deka\Order\Application\Services
 */
class CreateNewOrderHandler
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * @var OrderServiceInterface
     */
    private $orderService;

    /**
     * CreateNewOrderHandler constructor.
     * @param CommandBus $bus
     * @param OrderServiceInterface $orderService
     */
    public function __construct(CommandBus $bus, OrderServiceInterface $orderService)
    {
        $this->bus = $bus;
        $this->orderService = $orderService;
    }

    /**
     * @param CreateNewOrder $request
     * @return array
     */
    public function handle(CreateNewOrder $request)
    {
        try {
            return [
                'status' => 'success',
                'order' => [
                    'id' => $this->orderService->createOrder($request->toArray())
                ],
                'errors' => []
            ];
        } catch (Exception $exception) {
            return [
                'status' => 'failed',
                'error' => $exception->getMessage()
            ];
        }
    }
}

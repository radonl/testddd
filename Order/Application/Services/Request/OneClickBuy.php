<?php

namespace Deka\Order\Application\Services\Request;

use Deka\Order\Application\Validation\OneClickBuyRequestValidator;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class OneClickBuy
 * @package Deka\Order\Application\Services\Request
 */
class OneClickBuy {

    /**
     * @var string
     */
    private $telefon;

    /**
     * @var int
     */
    private $g_id;

    /**
     * @var int
     */
    private $count;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * OneClickBuy constructor.
     * @param array $request
     */
    public function __construct(array $request)
    {
        $this->telefon = $request['telefon'] ?? null;
        $this->g_id = $request['g_id'] ?? null;
        $this->count = $request['count'] ?? null;
        $this->validator = OneClickBuyRequestValidator::make($this->toArray());
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            'telefon' => $this->telefon,
            'g_id' => $this->g_id,
            'count' => $this->count
        ];
    }

    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }
}

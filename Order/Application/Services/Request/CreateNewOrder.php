<?php
namespace Deka\Order\Application\Services\Request;

use Deka\Order\Application\Validation\CreateNewOrderRequestValidator;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class CreateNewOrder
 * @package Deka\Order\Application\Services\Request
 */
class CreateNewOrder {

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $middlename;

    /**
     * @var string
     */
    private $telefon;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $address;

    /**
     * @var int
     */
    private $user_id;

    /**
     * @var string
     */
    private $certificate;

    /**
     * @var string
     */
    private $promoCode;

    /**
     * @var int
     */
    private $deliveryServiceId;

    /**
     * @var int
     */
    private $paymentMethodId;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * CreateNewOrder constructor.
     * @param array $request
     */
    public function __construct(array $request)
    {
        $this->lastname = $request['lastname'] ?? null;
        $this->firstname = $request['firstname'] ?? null;
        $this->middlename = $request['middlename'] ?? null;
        $this->telefon = $request['telefon'] ?? null;
        $this->email = $request['email'] ?? null;
        $this->address = $request['address'] ?? null;
        $this->user_id = $request['user_id'] ?? null;
        $this->certificate = $request['certificate'] ?? null;
        $this->promoCode = $request['promoCode'] ?? null;
        $this->deliveryServiceId = $request['deliveryServiceId'] ?? null;
        $this->paymentMethodId = $request['paymentMethodId'] ?? null;
        $this->validator = CreateNewOrderRequestValidator::make($this->toArray());
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            'lastname' => $this->lastname,
            'firstname' => $this->firstname,
            'middlename' => $this->middlename,
            'telefon' => $this->telefon,
            'email' => $this->email,
            'address' => $this->address,
            'user_id' => $this->user_id,
            'certificate' => $this->certificate,
            'promoCode' => $this->promoCode,
            'deliveryServiceId' => $this->deliveryServiceId,
            'paymentMethodId' => $this->paymentMethodId
        ];
    }

    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }
}

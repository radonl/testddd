<?php

namespace Deka\Order\Application\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;


/**
 * Class OneClickBuyRequestValidator
 * @package Deka\Order\Application\Validation
 */
class OneClickBuyRequestValidator extends FormRequest
{
    const RULES = [
        'telefon' => 'required|string|regex:/^\+380\(\d{2}\)\d{3}\-\d{2}\-\d{2}$/',
        'g_id' => 'required|numeric',
        'count' => 'required|numeric|min:1'
    ];

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($request){
        return Validator::make($request, OneClickBuyRequestValidator::RULES);
    }
}

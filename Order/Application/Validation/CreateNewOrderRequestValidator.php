<?php

namespace Deka\Order\Application\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

/**
 * Class CreateNewOrderRequestValidator
 * @package Deka\Order\Application\Validation
 */
class CreateNewOrderRequestValidator extends FormRequest
{

    const RULES = [
        'lastname' => 'required|string|max:255',
        'firstname' => 'required|string|max:255',
        'middlename' => 'required|string|max:255',
        'telefon' => 'required|string|regex:/^\+380\(\d{2}\)\d{3}\-\d{2}\-\d{2}$/',
        'email' => 'required|string|email|max:255',
        'address' => 'string|max:255',
        'deliveryServiceId' => 'required|int|delivery',
        'paymentMethodId' => 'required|int|paymentMethod',
        'certificate' => 'certificate',
        'promoCode' => 'promoCode'
    ];

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($request){
        return Validator::make($request, CreateNewOrderRequestValidator::RULES);
    }
}

<?php

namespace Deka\Order\Application\Validation\Rules;

use Deka\Certificate\Application\Services\Request\CheckCertificate;
use League\Tactician\CommandBus;

/**
 * Class CertificateRule
 * @package Deka\Order\Application\Validation\Rules
 */
class CertificateRule
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CertificateRule constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator) {
        if ($value) {
            $data = $validator->getData();
            $request = new CheckCertificate([
                'phone' => $data['telefon'] ?? null,
                'certificate' => $data['certificate'] ?? null
            ]);
            if ($request->getValidator()->fails()) {
                $validator->getMessageBag()->add('certificate', $request->getValidator()->errors()->toArray());
            } else {
                $res = $this->bus->handle($request);
                if (array_key_exists('error', $res) && array_key_exists('certificate', $res['error'])) {
                    $validator->getMessageBag()->add('certificate', $res['error']['certificate']);
                }
            }
        }
        return true;
    }
}

<?php

namespace Deka\Order\Application\Validation\Rules;

use Deka\Delivery\Application\Services\Request\SelectDelivery;
use League\Tactician\CommandBus;

/**
 * Class CertificateRule
 * @package Deka\Order\Application\Validation\Rules
 */
class DeliveryRule
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CertificateRule constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator) {
        $request = new SelectDelivery($value);
        if ($request->getValidator()->fails()) {
            $validator->getMessageBag()->add('deliveryServiceId', $request->getValidator()->errors()->toArray());
        } else {
            $res = $this->bus->handle($request);
            if (array_key_exists('error', $res)) {
                $validator->getMessageBag()->add('deliveryServiceId', $res['error']);
            }
        }
        return true;
    }
}

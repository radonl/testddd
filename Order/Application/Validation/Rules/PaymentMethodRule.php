<?php

namespace Deka\Order\Application\Validation\Rules;

use Carbon\Carbon;
use Deka\Payment\Application\Services\Request\SelectPaymentMethod;
use Illuminate\Support\Facades\DB;
use League\Tactician\CommandBus;

/**
 * Class CertificateRule
 * @package Deka\Order\Application\Validation\Rules
 */
class PaymentMethodRule
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CertificateRule constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator) {

        $res = $this->bus->handle(new SelectPaymentMethod($value));
        if (array_key_exists('error', $res)) {
            $validator->getMessageBag()->add('paymentMethodId', $res['error']);
        }
        return true;
    }
}

<?php

namespace Deka\Order\Application\Validation\Rules;

use Carbon\Carbon;
use Deka\PromoCode\Application\Services\Request\CheckPromoCode;
use Illuminate\Support\Facades\DB;
use League\Tactician\CommandBus;

/**
 * Class CertificateRule
 * @package Deka\Order\Application\Validation\Rules
 */
class PromoCodeRule
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * PromoCodeRule constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator) {
        if($value) {
            $request = new CheckPromoCode([
                'promoCode' => $value
            ]);
            if ($request->getValidator()->fails()) {
                $validator->getMessageBag()->add('promoCode', $request->getValidator()->errors()->toArray());
            } else {
                $res = $this->bus->handle($request);
                if (array_key_exists('error', $res) && array_key_exists('promoCode', $res['error'])) {
                    $validator->getMessageBag()->add('promoCode', $res['error']['promoCode']);
                }
            }
        }
        return true;
    }
}

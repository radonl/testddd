<?php

namespace Tests\Unit;

use Deka\Order\Domain\Interfaces\OrderEntityInterface;
use Deka\Order\Domain\Interfaces\OrderServiceInterface;
use League\Tactician\CommandBus;
use Tests\TestCase;

/**
 * Class TestCartService
 * @package Tests\Unit
 */
class TestOrderService extends TestCase
{
    /**
     * @var OrderServiceInterface
     */
    private $service;

    /**
     * @var \Mockery\MockInterface
     */
    private $orderEntityMock;

    /**
     * @var \Mockery\MockInterface
     */
    private $busMock;

    /**
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp():void {
        parent::setUp();
        $orderEntityMock = $this->mock(OrderEntityInterface::class);
        $this->app->bind('Deka\Order\Domain\Interfaces\OrderEntityInterface', function () use ($orderEntityMock) {
            return $orderEntityMock;
        });
        $this->orderEntityMock = $orderEntityMock;
        $busMock = $this->mock(CommandBus::class);
        $this->app->bind('League\Tactician\CommandBus', function () use ($busMock) {
            return $busMock;
        });
        $this->busMock = $busMock;
        $this->service = $this->app->make('\Deka\Order\Domain\Services\OrderService');
    }

    public function testCreateOrder() {
        $products = collect([]);
        $request = [];
        $this->busMock
            ->shouldReceive('handle')
            ->andReturn($products);
        $this->orderEntityMock
            ->shouldReceive('setData');
        $this->orderEntityMock
            ->shouldReceive('save');
        $this->assertTrue($this->service->createOrder($request) == 0);
    }
}

<?php

namespace Tests\Unit;

use Deka\Cart\Application\Validation\AddProductRequestValidator;
use Deka\Cart\Application\Validation\RemoveProductRequestValidator;
use Deka\Certificate\Application\Validation\CertificateCheckValidator;
use Deka\Delivery\Application\Validation\GetBranchesValidator;
use Deka\Order\Application\Validation\CreateNewOrderRequestValidator;
use Faker\Factory;
use Tests\TestCase;

/**
 * Class TestsUnitAddProductValidation
 * @package Tests\Unit
 */
class TestCreateNewOrderRequestValidation extends TestCase
{
    /**
     * @var array $rules
     */
    private $rules;

    /**
     * @var \Illuminate\Validation\Validator $validator
     */
    private $validator;

    private $data = [
        'lastname' => 'test',
        'firstname' => 'test',
        'middlename' => 'test',
        'telefon' => '+380(11)111-11-11',
        'email' => 'test@test.com',
        'address' => 'test',
    ];

    protected function setUp():void {
        parent::setUp();
        $this->validator = app()->get('validator');
        $this->rules = CreateNewOrderRequestValidator::RULES;
        unset($this->rules['deliveryServiceId']);
        unset($this->rules['paymentMethodId']);
        unset($this->rules['certificate']);
        unset($this->rules['promoCode']);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function validationProvider() {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);
        return [
            'request.success' => [
                'passed' => true,
                'request' => $this->data
            ],
            'lastname.validation.required' => [
                'passed' => false,
                'request' => array_merge($this->data, ['lastname' => ''])
            ],
            'lastname.validation.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['lastname' => 12345])
            ],
            'lastname.validation.max.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['lastname' => $faker->text(1000)])
            ],
            'lastname.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data, ['lastname' => $faker->name()])
            ],
            'firstname.validation.required' => [
                'passed' => false,
                'request' => array_merge($this->data, ['lastname' => ''])
            ],
            'firstname.validation.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['lastname' => 12345])
            ],
            'firstname.validation.max.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['lastname' => $faker->text(1000)])
            ],
            'firstname.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data, ['lastname' => $faker->name()])
            ],
            'middlename.validation.required' => [
                'passed' => false,
                'request' => array_merge($this->data, ['lastname' => ''])
            ],
            'middlename.validation.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['lastname' => 12345])
            ],
            'middlename.validation.max.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['lastname' => $faker->text(1000)])
            ],
            'middlename.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data, ['lastname' => $faker->name()])
            ],
            'email.validation.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['email' => 12344])
            ],
            'email.validation.email' => [
                'passed' => false,
                'request' => array_merge($this->data, ['email' => 'hello'])
            ],
            'email.validation.max.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['email' => str_random(255).'@deka.ua'])
            ],
            'email.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data, ['email' => $faker->email])
            ],
            'telefon.validation.required' => [
                'passed' => false,
                'request' => array_merge($this->data, ['telefon' => ''])
            ],
            'telefon.validation.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['telefon' => 12345])
            ],
            'telefon.validation.regex' => [
                'passed' => false,
                'request' => array_merge($this->data, ['telefon' => '+380'])
            ],
            'telefon.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data,['telefon' =>  '+380(11)111-11-11'])
            ],
            'address.validation.required' => [
                'passed' => false,
                'request' => array_merge($this->data, ['address' => ''])
            ],
            'address.validation.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['address' => 12345])
            ],
            'address.validation.max.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['address' => $faker->text(1000)])
            ],
            'address.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data, ['address' => $faker->name()])
            ],
        ];
    }

    /**
     * @test
     * @dataProvider validationProvider
     * @param bool $shouldPass
     * @param array $mockedRequestData
     */
    public function validation_results_as_expected($shouldPass, $mockedRequestData)
    {
        $this->assertEquals(
            $shouldPass,
            $this->validate($mockedRequestData)
        );
    }

    /**
     * @param $mockedRequestData
     * @return mixed
     */
    protected function validate($mockedRequestData)
    {
        return $this->validator
            ->make($mockedRequestData, $this->rules)
            ->passes();
    }
}

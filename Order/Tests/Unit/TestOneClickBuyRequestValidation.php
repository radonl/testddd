<?php

namespace Tests\Unit;

use Deka\Cart\Application\Validation\AddProductRequestValidator;
use Deka\Cart\Application\Validation\RemoveProductRequestValidator;
use Deka\Certificate\Application\Validation\CertificateCheckValidator;
use Deka\Delivery\Application\Validation\GetBranchesValidator;
use Deka\Order\Application\Validation\CreateNewOrderRequestValidator;
use Deka\Order\Application\Validation\OneClickBuyRequestValidator;
use Faker\Factory;
use Tests\TestCase;

/**
 * Class TestsUnitAddProductValidation
 * @package Tests\Unit
 */
class TestCreateNewOrderRequestValidation extends TestCase
{
    /**
     * @var array $rules
     */
    private $rules;

    /**
     * @var \Illuminate\Validation\Validator $validator
     */
    private $validator;

    private $data = [
        'telefon' => '+380(11)111-11-11',
        'g_id' => 1,
        'count' => 1
    ];

    protected function setUp():void {
        parent::setUp();
        $this->validator = app()->get('validator');
        $this->rules = OneClickBuyRequestValidator::RULES;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function validationProvider() {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);
        return [
            'request.success' => [
                'passed' => true,
                'request' => $this->data
            ],
            'telefon.validation.string' => [
                'passed' => false,
                'request' => array_merge($this->data, ['telefon' => 12345])
            ],
            'telefon.validation.regex' => [
                'passed' => false,
                'request' => array_merge($this->data, ['telefon' => '+380'])
            ],
            'telefon.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data,['telefon' =>  '+380(11)111-11-11'])
            ],
            'g_id.validation.required' => [
                'passed' => false,
                'request' => array_merge($this->data, ['g_id' => ''])
            ],
            'g_id.validation.numeric' => [
                'passed' => false,
                'request' => array_merge($this->data, ['g_id' => 'test'])
            ],
            'g_id.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data, ['g_id' => rand(1,100)])
            ],
            'count.validation.required' => [
                'passed' => false,
                'request' => array_merge($this->data, ['count' => ''])
            ],
            'count.validation.numeric' => [
                'passed' => false,
                'request' => array_merge($this->data, ['count' => 'test'])
            ],
            'count.validation.min' => [
                'passed' => false,
                'request' => array_merge($this->data, ['count' => rand(-100,-1)])
            ],
            'count.validation.success' => [
                'passed' => true,
                'request' => array_merge($this->data, ['count' => rand(1,100)])
            ]
        ];
    }

    /**
     * @test
     * @dataProvider validationProvider
     * @param bool $shouldPass
     * @param array $mockedRequestData
     */
    public function validation_results_as_expected($shouldPass, $mockedRequestData)
    {
        $this->assertEquals(
            $shouldPass,
            $this->validate($mockedRequestData)
        );
    }

    /**
     * @param $mockedRequestData
     * @return mixed
     */
    protected function validate($mockedRequestData)
    {
        return $this->validator
            ->make($mockedRequestData, $this->rules)
            ->passes();
    }
}

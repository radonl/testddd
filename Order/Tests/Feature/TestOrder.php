<?php

namespace Tests\Feature;

use App\Domain\Interfaces\CartServiceInterface;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use League\Tactician\CommandBus;
use Symfony\Component\HttpFoundation\Cookie;
use Tests\TestCase;

/**
 * Class TestCrossSale
 * @package Tests\Feature
 */
class TestDelivery extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var \Mockery\MockInterface
     */
    public $busMock;

    protected function setUp():void {
        parent::setUp();
        $bus = $this->mock(CommandBus::class);
        $this->app->bind('League\Tactician\CommandBus', function () use ($bus) {
            return $bus;
        });
        $this->busMock = $bus;
    }

    public function testOrder() {
        $this->busMock->shouldReceive('handle')
            ->andReturn(['test' => 'test']);
        $this->json('GET', '/api/checkout/order/', [], ['Authorization' => 'test'])
            ->assertJson(['test' => 'test']);
    }

    public function testNewOrder() {
        $this->busMock->shouldReceive('handle')
            ->andReturn([
                'test' => 'test',
                'status' => 'success'
            ]);
        $this->json('POST', '/api/checkout/order/new/', [
           'lastname' => 'test',
           'firstname' => 'test',
           'middlename' => 'test',
           'telefon' => '+380(11)111-11-11',
           'email' => 'test@test.com',
           'address' => 'test',
           'deliveryServiceId' => 1,
           'paymentMethodId' => 1
        ])->assertJson(['test' => 'test']);
    }

    public function testOneClickBuy() {
        $this->busMock->shouldReceive('handle')
            ->andReturn([
                'test' => 'test',
                'status' => 'success'
            ]);
        $this->json('POST', '/api/checkout/order/new/one-click-buy', [
            'telefon' => '+380(11)111-11-11',
            'g_id' => 1,
            'count' => 1
        ])->assertJson(['test' => 'test']);
    }
}

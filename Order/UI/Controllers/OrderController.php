<?php

namespace Deka\Order\UI\Controllers;


use Deka\Order\Application\Events\OrderPageVisited;
use Deka\Order\Application\Services\Request\CreateNewOrder;
use Deka\Order\Application\Services\Request\OneClickBuy;
use Deka\User\Application\Services\Request\GetUserInfo;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use League\Tactician\CommandBus;

/**
 * Class OrderController
 * @package Deka\Order\UI\Controllers
 */
class OrderController extends Controller
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * CommentsController constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus) {
        $this->bus = $bus;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function order(Request $request)
    {
        if ($request->header('Authorization')) {
            event(new OrderPageVisited());
            return response()->json($this->bus->handle(new GetUserInfo($request->header('Authorization'))), 200);
        } else {
            return response()->json([
                'status' => false
            ], 401);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newOrder(Request $request) {
        $request = new CreateNewOrder($request->toArray());
        if ($request->getValidator()->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $request->getValidator()->errors()
            ], 422);
        } else {
            $result = $this->bus->handle($request);
            return response()->json($result, $result['status'] == 'success' ? 200 : 422);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function oneClickBuy(Request $request) {
        $request = new OneClickBuy($request->toArray());
        if ($request->getValidator()->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $request->getValidator()->errors()
            ]);
        }
        $result = $this->bus->handle($request);
        return response()->json($result, $result["status"] == 'success' ? 200 : 422);
    }
}

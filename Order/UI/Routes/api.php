<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->middleware('api')
    ->namespace("Deka\Order\UI\Controllers")
    ->group(function () {
        Route::get('/checkout/order/', "OrderController@order");
        Route::post('/checkout/order/new/', "OrderController@newOrder");
        Route::post('/checkout/order/new/one-click-buy/', "OrderController@oneClickBuy");
    });

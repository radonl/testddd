<?php

namespace Deka\Order\Infrastructure\Repositories;

use Carbon\Carbon;
use Deka\Order\Infrastructure\Interfaces\GoodsWriteRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class GoodsWriteRepository
 * @package Deka\Order\Infrastructure\Repositories
 */
class GoodsWriteRepository implements  GoodsWriteRepositoryInterface {

    /**
     * @param Collection $goods
     * @param int $order_id
     */
    public function createGoods(Collection $goods, int $order_id): void
    {
        $now = Carbon::now();
        DB::table('checkout_goods')->insert($goods->map(function ($item, $key) use ($order_id, $now) {
            return [
                'order_id' => $order_id,
                'g_id' => $key,
                'count' => $item->getCount(),
                'price' => $item->getPrice(),
                'old_price' => $item->getOldPrice(),
                'date_order' => $now,
                'status' => 0,
                'zametka' => ''
            ];
        })->toArray());
    }
}

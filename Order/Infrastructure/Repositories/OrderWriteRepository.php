<?php

namespace Deka\Order\Infrastructure\Repositories;

use Carbon\Carbon;
use Deka\Order\Domain\Entity\OrderEntity;
use Deka\Order\Infrastructure\Interfaces\DeliveryWriteRepositoryInterface;
use Deka\Order\Infrastructure\Interfaces\GoodsReadRepositoryInterface;
use Deka\Order\Infrastructure\Interfaces\GoodsWriteRepositoryInterface;
use Deka\Order\Infrastructure\Interfaces\OrderWriteRepositoryInterface;
use Deka\Order\Infrastructure\Interfaces\PersonWriteRepositoryInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class OrderWriteRepository
 * @package Deka\Order\Infrastructure\Repositories
 */
class OrderWriteRepository implements  OrderWriteRepositoryInterface {

    /**
     * @var PersonWriteRepositoryInterface
     */
    private $personWriteRepository;

    /**
     * @var GoodsWriteRepositoryInterface
     */
    private $goodsWriteRepository;

    /**
     * @var GoodsReadRepository
     */
    private $goodsReadRepository;

    /**
     * @var DeliveryWriteRepositoryInterface
     */
    private $deliveryWriteRepository;

    /**
     * OrderWriteRepository constructor.
     * @param PersonWriteRepositoryInterface $personWriteRepository
     * @param GoodsWriteRepositoryInterface $goodsWriteRepository
     * @param GoodsReadRepositoryInterface $goodsReadRepository
     * @param DeliveryWriteRepositoryInterface $deliveryWriteRepository
     */
    public function __construct(
        PersonWriteRepositoryInterface $personWriteRepository,
        GoodsWriteRepositoryInterface $goodsWriteRepository,
        GoodsReadRepositoryInterface $goodsReadRepository,
        DeliveryWriteRepositoryInterface $deliveryWriteRepository
    )
    {
        $this->goodsWriteRepository = $goodsWriteRepository;
        $this->personWriteRepository = $personWriteRepository;
        $this->goodsReadRepository = $goodsReadRepository;
        $this->deliveryWriteRepository = $deliveryWriteRepository;
    }

    /**
     * @param OrderEntity $orderEntity
     * @return int
     */
    public function createOrder(OrderEntity $orderEntity): int
    {
         DB::transaction(function () use ($orderEntity, &$orderId) {
            $personId = $this->personWriteRepository->createPerson($orderEntity->getPerson());
            $orderId = DB::table('checkout_orders')->insertGetId([
                'date_order' => Carbon::now(),
                'promoCode' => $orderEntity->getPromoCodeId(),
                'certificate' => $orderEntity->getCertificateId(),
                'payment_method_id' => $orderEntity->getPaymentMethodId(),
                'id_type' => 0,
                'ko_sale' => 0,
                'person_id' => $personId
            ]);
            $this->goodsWriteRepository->createGoods($orderEntity->getProducts(), $orderId);
            if($orderEntity->getDeliveryServiceId()) {
                $this->deliveryWriteRepository->createDelivery(
                    $orderEntity->getProducts()->keys(),
                    $orderEntity->getDeliveryServiceId(),
                    $orderEntity->getPriceOfDelivery(),
                    $orderId
                );
            }
         });
         return $orderId;
    }
}

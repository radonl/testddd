<?php

namespace Deka\Order\Infrastructure\Repositories;

use Deka\Order\Infrastructure\Interfaces\GoodsReadRepositoryInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class GoodsReadRepository
 * @package Deka\Order\Infrastructure\Repositories
 */
class GoodsReadRepository implements GoodsReadRepositoryInterface {

    /**
     * @param int $order_id
     * @return array
     */
    public function getGoodsIds(int $order_id): array
    {
        return DB::table('checkout_goods')->where('order_id', $order_id)
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();
    }
}

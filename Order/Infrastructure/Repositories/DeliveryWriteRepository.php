<?php

namespace Deka\Order\Infrastructure\Repositories;

use Deka\Order\Domain\Entity\ProductEntity;
use Deka\Order\Infrastructure\Interfaces\DeliveryWriteRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class DeliveryWriteRepository
 * @package Deka\Order\Infrastructure\Repositories
 */
class DeliveryWriteRepository implements  DeliveryWriteRepositoryInterface {

    /**
     * @param Collection $productsIds
     * @param int $deliveryServiceId
     * @param int $priceOfDelivery
     * @param int $order_id
     */
    public function createDelivery(Collection $productsIds, int $deliveryServiceId, int $priceOfDelivery, int $order_id): void
    {
        DB::table('checkout_delivery')->insert($productsIds->map(function ($item) use ($deliveryServiceId, $order_id, $priceOfDelivery) {
            return [
                'order_id' => $order_id,
                'goods_order_id' => $item,
                'sluzhba' => $deliveryServiceId,
                'dostavka_price' => $priceOfDelivery
            ];
        })->toArray());
    }
}

<?php

namespace Deka\Order\Infrastructure\Repositories;

use Deka\Order\Domain\Entity\PersonEntity;
use Deka\Order\Infrastructure\Interfaces\PersonWriteRepositoryInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class PersonWriteRepository
 * @package Deka\Order\Infrastructure\Repositories
 */
class PersonWriteRepository implements  PersonWriteRepositoryInterface {

    /**
     * @param PersonEntity $personEntity
     * @return int
     */
    public function createPerson(PersonEntity $personEntity): int
    {
        return DB::table('checkout_persons')->insertGetId([
            'lastname' => $personEntity->getLastname(),
            'firstname' => $personEntity->getFirstname(),
            'middlename' => $personEntity->getMiddlename(),
            'telefon' => $personEntity->getTelefon(),
            'email' => $personEntity->getEmail(),
            'delivery_type' => $personEntity->getSluzhba(),
            'adress' => $personEntity->getAdress(),
            'user_id' => $personEntity->getUserId()
        ]);
    }
}

<?php


namespace Deka\Order\Infrastructure\Interfaces;


use Illuminate\Support\Collection;

/**
 * Interface GoodsWriteRepositoryInterface
 * @package Deka\Order\Infrastructure\Interfaces
 */
interface GoodsWriteRepositoryInterface
{

    /**
     * @param Collection $goods
     * @param int $order_id
     */
    public function createGoods(Collection $goods, int $order_id): void;
}

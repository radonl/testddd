<?php

namespace Deka\Order\Infrastructure\Interfaces;


use Deka\Order\Domain\Entity\PersonEntity;

/**
 * Interface PersonWriteRepositoryInterface
 * @package Deka\Order\Infrastructure\Interfaces
 */
interface PersonWriteRepositoryInterface
{
    /**
     * @param PersonEntity $personEntity
     * @return int
     */
    public function createPerson(PersonEntity $personEntity): int;
}

<?php

namespace Deka\Order\Infrastructure\Interfaces;

/**
 * Interface GoodsReadRepositoryInterface
 * @package Deka\Order\Infrastructure\Interfaces
 */
interface GoodsReadRepositoryInterface
{

    /**
     * @param int $order_id
     * @return array
     */
    public function getGoodsIds(int $order_id): array;
}

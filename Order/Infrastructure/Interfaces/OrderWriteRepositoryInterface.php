<?php

namespace Deka\Order\Infrastructure\Interfaces;

use Deka\Order\Domain\Entity\OrderEntity;

/**
 * Interface OrderWriteRepositoryInterface
 * @package Deka\Order\Infrastructure\Interfaces
 */
interface OrderWriteRepositoryInterface
{
    /**
     * @param OrderEntity $product
     * @return int
     */
    public function createOrder(OrderEntity $product): int;
}

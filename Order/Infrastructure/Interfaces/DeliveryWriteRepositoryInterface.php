<?php


namespace Deka\Order\Infrastructure\Interfaces;

use Illuminate\Support\Collection;

/**
 * Interface DeliveryWriteRepositoryInterface
 * @package Deka\Order\Infrastructure\Interfaces
 */
interface DeliveryWriteRepositoryInterface
{

    /**
     * @param Collection $productsIds
     * @param int $deliveryServiceId
     * @param int $priceOfDelivery
     * @param int $order_id
     */
    public function createDelivery(Collection $productsIds, int $deliveryServiceId, int $priceOfDelivery, int $order_id): void;
}

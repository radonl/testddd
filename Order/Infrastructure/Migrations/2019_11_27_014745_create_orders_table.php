<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateOrdersTable
 */
class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkout_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('date_order');
            $table->unsignedBigInteger('person_id')->nullable();
            $table->enum('payment_type', ['card', 'credit']);
            $table->string('note')->nullable();
            $table->integer('promoCode')->nullable();
            $table->integer('certificate')->nullable();
            $table->integer('fishka')->nullable();
            $table->unsignedBigInteger('id_type');
            $table->integer('ko_sale');
            $table->boolean('is_one_click_buy')->default(0);
        });
        Schema::table('checkout_orders', function(Blueprint $table) {
            $table->foreign('person_id')->references('id')->on('checkout_persons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checkout_orders', function (Blueprint $table) {
            $table->dropForeign(['person_id']);
        });
        Schema::dropIfExists('checkout_orders');
    }
}
